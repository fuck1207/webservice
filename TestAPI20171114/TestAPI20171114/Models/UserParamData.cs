﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestAPI20171114.Models
{
    [Serializable]
    public class UserParamData
    {
        /// <summary>
        /// 会员ID
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// 会员账号
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 商户ID
        /// </summary>
        public string IdentityId { get; set; }

        /// <summary>
        /// 是否登录 0 未登录,1 已登录
        /// </summary>
        public int IsLogin { get; set; }

        /// <summary>
        /// 最后登录地址
        /// </summary>
        public string LastLoginAddr { get; set; }

        /// <summary>
        /// 设备ID 1(PC),2(APP,Mobile)
        /// </summary>
        public int SourceId { get; set; }

        /// <summary>
        /// 设备名称 APP Mobile PC
        /// </summary>
        public string SourceName { get; set; }

        /// <summary>
        /// 会员登入的网址
        /// </summary>
        public string SourceUrl { get; set; }

        /// <summary>
        /// 用户信息
        /// </summary>
        //public dt_ssc_users DtUsers { get; set; }

        /// <summary>
        /// 返点信息
        /// </summary>
        public Dictionary<string, decimal> DisPoint { get; set; }
    }
}