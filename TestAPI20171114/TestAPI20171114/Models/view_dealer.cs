﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestAPI20171114.Models
{
    public class view_dealer
    {
        [ReportDescriptionAttribute("id")]
        public int ID { get; set; }

        [ReportDescriptionAttribute("dealerId")]
        public string Code { get; set; }

        [ReportDescriptionAttribute("dealerName")]
        public string Name { get; set; }

        [ReportDescriptionAttribute("area")]
        public string City { get; set; }

        [ReportDescriptionAttribute("age")]
        public byte? Age { get; set; }

        [ReportDescriptionAttribute("sex")]
        public byte? Sex { get; set; }

        [ReportDescriptionAttribute("height")]
        public decimal? Height { get; set; }

        [ReportDescriptionAttribute("weight")]
        public decimal? Weight { get; set; }

        [ReportDescriptionAttribute("bwh")]
        public string BWH { get; set; }

        [ReportDescriptionAttribute("img")]
        public string Image { get; set; }

        [ReportDescriptionAttribute("img2")]
        public string Photo { get; set; }

        [ReportDescriptionAttribute("add_time")]
        public DateTime? AddTime { get; set; }
    }
}