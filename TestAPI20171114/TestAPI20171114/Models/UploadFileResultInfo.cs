﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestAPI20171114.Models
{
    public class UploadFileResultInfo
    {
        /// <summary>
        /// 上传文件名称（前端传来的参数名称）
        /// </summary>
        public string UploadFileName { get; set; }
        /// <summary>
        /// 数据库字段名称
        /// </summary>
        public string DBFileName { get; set; }
        /// <summary>
        /// 删除标识字段名称（前端传来的参数名称）
        /// </summary>
        public string DeleteIdentifyFileName { get; set; }
        /// <summary>
        /// 删除标识接收前端返回值
        /// </summary>
        public string DeleteIdentifyValue { get; set; }
        /// <summary>
        /// 生成的文件名称
        /// </summary>
        public string GenerateFileName { get; set; }
        /// <summary>
        /// 限制字段名称
        /// </summary>
        public string LimitFileName { get; set; }
        /// <summary>
        /// 限制宽度，0不限制
        /// </summary>
        public int LimitWidth { get; set; }
        /// <summary>
        /// 限制高度，0不限制
        /// </summary>
        public int LimitHeight { get; set; }
        /// <summary>
        /// 返回的错误信息
        /// </summary>
        public string strErrorMsg { get; set; }
        /// <summary>
        /// 提示名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 存放图片的文件路径
        /// </summary>
        public string FilePath { get; set; }
        /// <summary>
        /// 重新命名的文件名称
        /// </summary>
        public string RenameFileName { get; set; }
    }
}