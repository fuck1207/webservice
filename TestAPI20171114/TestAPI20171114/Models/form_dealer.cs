﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestAPI20171114.Models
{
    public class form_dealer
    {
        [ReportDescriptionAttribute("ID")]
        public int id { get; set; }

        [ReportDescriptionAttribute("Code")]
        public string dealerId { get; set; }

        [ReportDescriptionAttribute("Name")]
        public string dealerName { get; set; }

        [ReportDescriptionAttribute("City")]
        public string area { get; set; }

        [ReportDescriptionAttribute("Age")]
        public byte age { get; set; }

        [ReportDescriptionAttribute("Sex")]
        public byte sex { get; set; }

        [ReportDescriptionAttribute("Height")]
        public decimal height { get; set; }

        [ReportDescriptionAttribute("Weight")]
        public decimal weight { get; set; }

        [ReportDescriptionAttribute("BWH")]
        public string bwh { get; set; }

        [ReportDescriptionAttribute("Image")]
        public string img { get; set; }

        
        [ReportDescriptionAttribute("Photo")]
        public string img2 { get; set; }

        [ReportDescriptionAttribute("add_time")]
        public System.DateTime add_time { get; set; }

        [ReportDescriptionAttribute("update_time")]
        public System.DateTime update_time { get; set; }

        [ReportDescriptionAttribute("lockid")]
        public byte[] lockid { get; set; }
    }
    public class ReportDescriptionAttribute : Attribute
    {
        public string Description { get; set; }

        public ReportDescriptionAttribute(string Description)
        {
            this.Description = Description;
        }

        public override string ToString()
        {
            return this.Description.ToString();
        }
    }
}