﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestAPI20171114.Models
{
    public class form_manager
    {
        [ReportDescriptionAttribute("ID")]
        public int id { get; set; }
        [ReportDescriptionAttribute("UserName")]
        public string user_name { get; set; }
        [ReportDescriptionAttribute("Password")]
        public string password { get; set; }
        [ReportDescriptionAttribute("RealName")]
        public string real_name { get; set; }
        [ReportDescriptionAttribute("AdminRole")]
        public int admin_role { get; set; }
        public class ReportDescriptionAttribute : Attribute
        {
            public string Description { get; set; }

            public ReportDescriptionAttribute(string Description)
            {
                this.Description = Description;
            }

            public override string ToString()
            {
                return this.Description.ToString();
            }
        }
    }
}