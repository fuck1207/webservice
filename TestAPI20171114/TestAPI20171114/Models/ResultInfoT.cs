﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestAPI20171114.Models
{
    public class ResultInfoT<T>
    {
        /// <summary>
        /// 1正常返回,且已登录
        /// 0正常返回,且未登录
        /// 2用于提示需要验证码了(仅登录使用)
        /// -1传入信息有误
        /// -2后台处理错误
        /// -7系统维护
        /// -8账户冻结
        /// -9返点验证不通过(仅投注使用)
        /// </summary>
        public int Code
        {
            get;
            set;
        }
        public string StrCode
        {
            get;
            set;
        }
        public int DataCount
        {
            get;
            set;
        }
        public string BackUrl
        {
            get;
            set;
        }
        public T BackData
        {
            get;
            set;
        }
        public T Data
        {
            get;
            set;
        }
        public T CacheData
        {
            get;
            set;
        }

        /// <summary>
        /// 第三方类型  0  一般  1  新开窗口  2  生成二维码 3  64位图片
        /// </summary>
        public int OpenType
        {
            get;
            set;
        }
        /// <summary>
        /// 新增Style一项来告诉WAP版打开页面后如何定位
        /// </summary>
        public string Style
        {
            get;
            set;
        }
        public bool IsLogin
        {
            get;
            set;
        }

    }
}