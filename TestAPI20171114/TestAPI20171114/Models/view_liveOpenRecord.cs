﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestAPI20171114.Models
{

    public class view_liveOpenRecord
    {
        [ReportDescriptionAttribute("id")]
        public int ID { get; set; }

        [ReportDescriptionAttribute("code")]
        public string Code { get; set; }

        [ReportDescriptionAttribute("issueNo")]
        public string Issue { get; set; }

        [ReportDescriptionAttribute("startTime")]
        public DateTime? StartTime { get; set; }

        [ReportDescriptionAttribute("endTime")]
        public DateTime? EndTime { get; set; }

        [ReportDescriptionAttribute("userName")]
        public string Name { get; set; }

        [ReportDescriptionAttribute("openNum")]
        public string Result { get; set; }

        [ReportDescriptionAttribute("add_time")]
        public DateTime? Time { get; set; }
        

    }

}