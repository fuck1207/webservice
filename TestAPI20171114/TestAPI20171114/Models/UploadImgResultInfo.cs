﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestAPI20171114.Models
{
    public class UploadImgResultInfo
    {
        /// <summary>
        /// 上传文件名称（前端传来的参数名称）
        /// </summary>
        public string UploadFileName { get; set; }
        /// <summary>
        /// 存放图片的文件路径
        /// </summary>
        public string FilePath { get; set; }
        /// <summary>
        /// 前端傳的base64
        /// </summary>
        public string Base64 { get; set; }
        /// <summary>
        /// 副檔名
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// 狀態
        /// </summary>
        public int State { get; set; }
    }
}