﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestAPI20171114.Models.Request
{
    public class GetRoleInfo
    {
        public int Id { get; set; }

        public string RoleName { get; set; }

        public GetRoleInfoBodyData BodyData { get; set; }
    }

    public class GetRoleInfoBodyData
    {
        public string livecmsManage { get; set; }

        public string dealerManage { get; set; }

        public string dealerList { get; set; }

        public string dealerPost { get; set; }

        public string dealerTime { get; set; }

        public string liveManage { get; set; }

        public string videoList { get; set; }

        public string barrageManage { get; set; }

        public string systemBarrage { get; set; }

        public string systemFace { get; set; }

        public string safeWords { get; set; }

        public string specialWords { get; set; }

        public string whiteList { get; set; }

        public string blackList { get; set; }

        public string wordsManage { get; set; }

        public string manualReview { get; set; }

        public string giftManage { get; set; }

        public string giftList { get; set; }

        public string dealerTable { get; set; }

        public string Manager { get; set; }

        public string managerList { get; set; }

        public string roleManage { get; set; }

        public string manageLog { get; set; }
    }
}