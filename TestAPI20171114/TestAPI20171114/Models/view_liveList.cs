﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestAPI20171114.Models
{
    public class view_liveList
    {
        [ReportDescriptionAttribute("id")]
        public int ID { get; set; }

        [ReportDescriptionAttribute("code")]
        public string Code { get; set; }

        [ReportDescriptionAttribute("tableId")]
        public string LiveName { get; set; }

        [ReportDescriptionAttribute("liveId")]
        public string Title { get; set; }

        [ReportDescriptionAttribute("dealerId")]
        public string Name { get; set; }

        [ReportDescriptionAttribute("state")]
        public string State { get; set; }
        
        [ReportDescriptionAttribute("add_time")]
        public DateTime Time { get; set; }

        //[ReportDescriptionAttribute("stop_time")]
        //public string StopTime { get; set; }

        //[ReportDescriptionAttribute("StateValue")]
        //public string StateValue { get; set; }

    }
}