﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Text.RegularExpressions;
using TestAPI20171114.Models;

namespace TestAPI20171114.Controllers
{
    public class delManagerController : ApiController
    {
        [HttpPost]
        public ResultInfoT<object> Post()
        {
            var session = HttpContext.Current.Session;
            var request = HttpContext.Current.Request;
            var result = new ResultInfoT<object>();

            try
            {
                if (session["Login"] != null && session["Login"].ToString().TrimEnd() == "1")
                {
                    var managerId = int.Parse(session["ManagerId"].ToString());
                    
                    var multiId = request.Form["ID"] ?? "";

                    if (!string.IsNullOrEmpty(multiId) && new Regex(@"^(([\d]{1,}){1}|(([\d]{1,}\,){1,}([\d]{1,}){1}))$").IsMatch(multiId))
                    {
                        var idList = multiId.Split(',').Select(o => Convert.ToInt32(o)).Distinct().OrderBy(o => o).ToList();

                        using (var db = new livecloudEntities1())
                        {

                            var removeEntry = db.dt_Manager.RemoveRange(db.dt_Manager.Where(o => idList.Contains(o.id)));
                            
                            if (removeEntry.Count() == idList.Count)
                            {
                                db.SaveChanges();

                                result.IsLogin = CodeConstantsHelper.RetIsLogin;
                                result.Code = CodeConstantsHelper.RetGetDataSuc;
                                result.StrCode = CodeConstantsHelper.RetGetDataSucStr;
                            }
                            else
                            {

                                throw new Exception("delManager: entity remove fail");
                            }
                        }
                    }
                    else
                    {
                        result.Code = CodeConstantsHelper.RetParamErr;
                        result.StrCode = "參數錯誤";
                        result.IsLogin = CodeConstantsHelper.RetIsLogin;
                    }
                }
                else
                {
                    result.Code = CodeConstantsHelper.RetParamErr;
                    result.StrCode = CodeConstantsHelper.UserIsNotLoginStr;
                    result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                }

                return result;
            }
            catch (Exception e)
            {
                result.Code = CodeConstantsHelper.RetParamErr;
                result.StrCode = "執行時出錯";
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                Log.Error("setPassword", "setPassword", e.ToString());

                return result;
            }
        }

    }
}
