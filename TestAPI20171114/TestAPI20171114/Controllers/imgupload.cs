﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using TestAPI20171114.Models;

namespace TestAPI20171114.Controllers
{
    public class imgupload
    {
        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="uploadFileResultInfo">上传图片结果信息</param>
        /// <param name="Request">Http请求</param>
        /// <param name="strIdentityId">站长ID</param>
        /// <returns></returns>
        //public string UploadImage(List<UploadFileResultInfo> uploadFileResultInfo, HttpRequestBase Request, string strIdentityId)
        //{
        //    try
        //    {
        //        //图片限制宽度
        //        //DataTable imageLimitData = GetImageLimitData();
        //        for (int i = 0; i < uploadFileResultInfo.Count; i++)
        //        {
        //            string deleteIdentityFileName = uploadFileResultInfo[i].DeleteIdentifyFileName;
        //            if (!string.IsNullOrWhiteSpace(deleteIdentityFileName))
        //            {
        //                string strDeleteIdentityValue = Request[uploadFileResultInfo[i].DeleteIdentifyFileName];
        //                uploadFileResultInfo[i].DeleteIdentifyValue = strDeleteIdentityValue;
        //            }

        //            string strUploadFileName = uploadFileResultInfo[i].UploadFileName;
        //            if (string.IsNullOrWhiteSpace(strUploadFileName))
        //                continue;

        //            string strResult = string.Empty;
        //            HttpPostedFileBase uploadFile = Request.Files[uploadFileResultInfo[i].UploadFileName];
        //            if (uploadFile == null)
        //                continue;

        //            //图片限制宽度
        //            string strImageWidth = string.Empty;
        //            //图片限制高度
        //            string strImageHeight = string.Empty;
        //            DataRow[] dataRows = imageLimitData.Select("TypeName='" + uploadFileResultInfo[i].LimitFileName + "'");
        //            if (dataRows != null && dataRows.Length > 0)
        //            {
        //                object bjPCWidth = dataRows[0]["PCWidth"];
        //                if (bjPCWidth != null)
        //                {
        //                    strImageWidth = bjPCWidth.ToString();
        //                }
        //                object bjPCHeight = dataRows[0]["PCHeight"];
        //                if (bjPCHeight != null)
        //                {
        //                    strImageHeight = bjPCHeight.ToString();
        //                }
        //            }
        //            //限制宽度或高度时，则验证图片宽度或高度
        //            //文件大小
        //            int fileSize = uploadFile.ContentLength;
        //            Byte[] FileByteArray = new Byte[fileSize]; //图象文件临时储存Byte数组
        //            //建立数据流对像
        //            using (Stream StreamObject = uploadFile.InputStream)
        //            {
        //                //读取图象文件数据，FileByteArray为数据储存体，0为数据指针位置、FileLnegth为数据长度
        //                StreamObject.Read(FileByteArray, 0, fileSize);
        //                if (!string.IsNullOrWhiteSpace(strImageWidth) || !string.IsNullOrWhiteSpace(strImageHeight))
        //                {
        //                    Image image = Image.FromStream(StreamObject);
        //                    if (image == null)
        //                    {
        //                        uploadFileResultInfo[i].strErrorMsg = "未知图片格式！";
        //                        continue;
        //                    }
        //                    //图片宽度
        //                    int iImageWidth = 0;
        //                    if (!string.IsNullOrWhiteSpace(strImageWidth))
        //                    {
        //                        iImageWidth = int.Parse(strImageWidth);
        //                    }
        //                    //图片高度
        //                    int iImageHeight = 0;
        //                    if (!string.IsNullOrWhiteSpace(strImageHeight))
        //                    {
        //                        iImageHeight = int.Parse(strImageHeight);
        //                    }
        //                    if (iImageWidth > 0 && iImageWidth != image.Width || iImageHeight > 0 && iImageHeight != image.Height)
        //                    {
        //                        string strMsg = string.Empty;
        //                        if (iImageWidth > 0)
        //                        {
        //                            strMsg = "宽度：" + iImageWidth + "px";
        //                        }
        //                        if (iImageHeight > 0)
        //                        {
        //                            strMsg += " 高度：" + iImageHeight + "px";
        //                        }
        //                        string strTempErrorMsg = string.Empty;
        //                        if (!string.IsNullOrWhiteSpace(uploadFileResultInfo[i].Name))
        //                        {
        //                            uploadFileResultInfo[i].strErrorMsg = "[" + uploadFileResultInfo[i].Name + "]" + "图片尺寸要求[" + strMsg + "]！";
        //                            continue;
        //                        }
        //                        uploadFileResultInfo[i].strErrorMsg = "图片尺寸要求[" + strMsg + "]！";
        //                        continue;
        //                    }

        //                }
        //                string fileName = uploadFile.FileName;
        //                //文件扩展名
        //                string fileExtension = Path.GetExtension(fileName).ToLower();
        //                if (!(fileExtension == ".png" || fileExtension == ".gif" || fileExtension == ".jpg" || fileExtension == ".jpeg"))
        //                {
        //                    uploadFileResultInfo[i].strErrorMsg = "图片类型只能为gif,png,jpg,jpeg！";
        //                    continue;
        //                }
        //                if (fileSize > (int)(500 * 1024))
        //                {
        //                    uploadFileResultInfo[i].strErrorMsg = "图片大小不能超过500KB！";
        //                    continue;
        //                }
        //                Random random = new Random();
        //                //生成文件名
        //                string uploadFileName = DateTime.Now.ToString("yyyyMMddhhmmss") + random.Next(100000, 999999) + ".png";
        //                if (!string.IsNullOrWhiteSpace(uploadFileResultInfo[i].RenameFileName))
        //                {
        //                    uploadFileName = uploadFileResultInfo[i].RenameFileName;
        //                }
        //                //string uploadFilePath = "/" + strIdentityId + "/" + strFilePath + "/";
        //                string strFilePath = "UploadImages";
        //                if (!string.IsNullOrWhiteSpace(uploadFileResultInfo[i].FilePath))
        //                {
        //                    strFilePath = uploadFileResultInfo[i].FilePath;
        //                }
        //                string uploadFilePath = "/" + strIdentityId + "/" + strFilePath + "/"; //"/dafa/common/";
        //                string[] fileDirectory = uploadFilePath.Split('/');
        //                string strFileDirectory = string.Empty;
        //                for (int j = 0; j < fileDirectory.Length; j++)
        //                {
        //                    if (j == 0)
        //                    {
        //                        strFileDirectory += "/" + fileDirectory[j] + "/";
        //                    }
        //                    else
        //                    {
        //                        strFileDirectory += fileDirectory[j] + "/";
        //                    }
        //                    string directoryPath = System.Web.HttpContext.Current.Server.MapPath(strFileDirectory);
        //                    if (!Directory.Exists(directoryPath))//不存在这个文件夹就创建这个文件夹 
        //                    {
        //                        Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(strFileDirectory));
        //                    }
        //                }
        //                uploadFileResultInfo[i].GenerateFileName = uploadFilePath + uploadFileName;
        //                //EasyFrame.Web.Admin.imageserver.MySoapHeader mySoapHeader = new imageserver.MySoapHeader();
        //                ImageService.ImageService.MySoapHeader mySoapHeader = new ImageService.ImageService.MySoapHeader();
        //                mySoapHeader.UserName = Config.Config.ImageUserName;
        //                mySoapHeader.UserPwd = Config.Config.ImagePassword;
        //                ImageService.ImageService.ImagesServiceSoapClient mp = new ImageService.ImageService.ImagesServiceSoapClient();
        //                //mp.SaveFile(FileByteArray, fileSize, uploadFilePath.Replace("/", "\\") + GTRequest.GetQueryString("filename").ToString() + ".png");
        //                //context.Response.Write("{ \"msg\":\"上传成功！\", \"name\":\"" + uploadFilePath + GTRequest.GetQueryString("filename").ToString() + ".png" + "\",\"status\":\"1\" }");
        //                bool bIsOK = mp.SaveFile(mySoapHeader, FileByteArray, fileSize, uploadFilePath.Replace("/", "\\") + uploadFileName);
        //                if (!bIsOK)
        //                {
        //                    uploadFileResultInfo[i].strErrorMsg = "上传失败！";
        //                    continue;
        //                }
        //            }
        //        }
        //        return string.Empty;
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Info("UploadImage:" + LogString);
        //        Log.Error(ex);
        //        return EnumHelp.GetEnumDesc(Message.Error, "Error");
        //    }
        //}
    }
}