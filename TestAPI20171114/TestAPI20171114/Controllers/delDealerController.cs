﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TestAPI20171114.Models;

namespace TestAPI20171114.Controllers
{
    public class delDealerController : ApiController
    {
        Models.livecloudEntities1 db = new livecloudEntities1();

        [HttpPost]
        public ResultInfoT<object> Post()
        {
            ResultInfoT<object> result = new ResultInfoT<object>();
            var session = HttpContext.Current.Session;
            HttpRequest staticContext = HttpContext.Current.Request;
            string[] ID = staticContext.Form["ID"] != null ? staticContext.Form["ID"].ToString().TrimEnd().Split(',') : "".Split(',');
     
            try
            {
                if (session["Login"] != null && session["Login"].ToString().TrimEnd() == "1")
                {
                    using (db)
                    {
                        var managerId = int.Parse(HttpContext.Current.Session["ManagerId"].ToString());
                        foreach (string del_id in ID)
                        {
                            int del = Convert.ToInt32(del_id);
                            dt_dealer dt_dealer = (from s in db.dt_dealer where s.id == del select s).FirstOrDefault();
                            if(dt_dealer != null)
                            {
                                db.dt_dealer.Remove(dt_dealer);
                                var manageLog = new dt_ManageLog()
                                {
                                    ManagerId = managerId,
                                    ManagerName = session["ManagerName"].ToString(),
                                    ActionType = "delDealer",
                                    AddTime = DateTime.Now,
                                    Remarks = "刪除荷官:" + dt_dealer.dealerId + " " + dt_dealer.dealerName,
                                    IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]
                                };
                                db.dt_ManageLog.Add(manageLog);
                                db.SaveChanges();
                            }
                        }
                    }
                    result.Code = CodeConstantsHelper.RetGetDataSuc;
                    result.StrCode = "成功信息";
                    result.IsLogin = CodeConstantsHelper.RetIsLogin;
                }
                else
                {
                    result.Code = CodeConstantsHelper.RetParamErr;
                    result.StrCode = CodeConstantsHelper.UserIsNotLoginStr;
                    result.IsLogin = CodeConstantsHelper.RetIsNoLogin;

                }
            }
            catch
            {
                result.Code = CodeConstantsHelper.RetParamErr;
                result.StrCode = "错误信息";
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
            }
            return result;
        }
    }
}
