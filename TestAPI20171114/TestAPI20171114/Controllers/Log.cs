﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace TestAPI20171114.Controllers
{
    public static class Log
    {
        private static object _ErrorLogLock = new object();
        private static string ErrorLogPath = System.Threading.Thread.GetDomain().BaseDirectory + "\\Log\\Error\\";
        private static void Error(string floder,string title,string text)
        {
            try
            {
                var path = ErrorLogPath + floder + "\\";
                var msg = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "\t" + title + "\t" + text + Environment.NewLine;
                lock(_ErrorLogLock)
                {
                    if(!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    File.AppendAllText(path+DateTime.Now.ToString("yyyy-MM-dd") + ".log", msg);
                }
            }
            catch
            {

            }
        }
        public static void Error(string folder, string title, string text, string paras = " - ")
        {
            text += "\t" + paras;
            Error(folder, title, text);
        }

        private static object _LogLock = new object();
        private static string LogPath = System.Threading.Thread.GetDomain().BaseDirectory + "Log\\Log\\";
        private static void UseLog(string floder,string title,string text)
        {
            try
            {
                var path = LogPath + floder + "\\";
                var msg = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "\t" + title + "\t" + text + Environment.NewLine;
                lock(_LogLock)
                {
                    if(!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    File.AppendAllText(path + DateTime.Now.ToString("yyyy-MM-dd") + ".log", msg);
                }
            }
            catch
            {

            }
        }
        public static void UseLog(string folder, string title,string text,string paras = " - ")
        {
            text += "\t" + paras;
            UseLog(folder, title, text);
        }


    }
}