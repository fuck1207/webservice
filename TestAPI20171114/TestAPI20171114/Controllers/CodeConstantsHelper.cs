﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestAPI20171114.Controllers
{
    public class CodeConstantsHelper
    {
        /// <summary>
        /// 操作成功
        /// </summary>
        public const int RetSetSuc = 1;
        /// <summary>
        /// 1正常返回,且已登录
        /// </summary>
        public const int RetGetDataSuc = 1;
        public const string RetGetDataSucStr = "成功信息";
        /// <summary>
        ///  -1传入信息有误
        /// </summary>
        public const int RetParamErr = -1;
        public const string RetParamErrStr = "错误信息";
        /// <summary>
        ///  0正常返回,且未登录
        /// </summary>
        public const bool RetIsNoLogin = false;
        public const string UserIsNotLoginStr = "用户未登录！";

        /// <summary>
        ///  1正常返回,且登录
        /// </summary>
        public const bool RetIsLogin = true;
        public const string UserIsLoginStr = "用户登录！";


    }
}