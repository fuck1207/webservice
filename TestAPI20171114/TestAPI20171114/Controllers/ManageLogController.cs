﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestAPI20171114.Models;
using System.Web;
using TestAPI20171114.Models.Request;

namespace TestAPI20171114.Controllers
{
    public class ManageLogController : ApiController
    {
        Models.livecloudEntities1 db = new Models.livecloudEntities1();
        [HttpPost]
        public ResultInfoT<object> Post()
        {
            var session = HttpContext.Current.Session;
            var request = HttpContext.Current.Request;
            var result = new ResultInfoT<object>();

            try
            {
                if (session["Login"] != null && session["ManagerId"] != null && session["Login"].ToString().TrimEnd() == "1")
                {
                    var managerId = int.Parse(session["ManagerId"].ToString());

                    var userName = request.Form["UserName"] ?? "";
                    var actionType = request.Form["Type"] ?? "";
                    int pageSize = int.TryParse(request.Form["PageSize"], out pageSize) ? pageSize : -1;
                    int pageIndex = int.TryParse(request.Form["PageIndex"], out pageIndex) ? pageIndex : -1;
                    int skipRows = pageSize * pageIndex;

                    // 檢查參數
                    if (pageSize < 0 || pageIndex < 0)
                    {
                        throw new Exception("自訂檢查 參數錯誤");
                    }

                    // 進DB
                    using (var db = new livecloudEntities1())
                    {
                        var query = from o in db.dt_ManageLog
                                    select o;

                        if (!string.IsNullOrEmpty(userName))
                            query = query.Where(o => o.ManagerName == userName);

                        if (!string.IsNullOrEmpty(actionType))
                            query = query.Where(o => o.ActionType == actionType);

                        var totalCount = query.Count();

                        var list = query.OrderByDescending(o=>o.Id).Skip(skipRows).Take(pageSize).ToList().Select(o => new ManageLog()
                        {
                            IP = o.IP,
                            ID = o.Id,
                            Mark = o.Remarks,
                            Time = o.AddTime.ToString("yyyy-MM-dd HH:mm:ss"),
                            Type = o.ActionType,
                            UserName = o.ManagerName
                        }).ToList();

                        result.DataCount = totalCount;
                        result.BackData = list;
                    }
                    

                    result.IsLogin = CodeConstantsHelper.RetIsLogin;
                    result.Code = CodeConstantsHelper.RetGetDataSuc;
                    result.StrCode = CodeConstantsHelper.RetGetDataSucStr;
                }
                else
                {
                    result.Code = CodeConstantsHelper.RetParamErr;
                    result.StrCode = CodeConstantsHelper.UserIsNotLoginStr;
                    result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                }
                return result;
            }
            catch (Exception e)
            {
                result.Code = CodeConstantsHelper.RetParamErr;
                result.StrCode = "執行時出錯";
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                Log.Error("ManageLog", e.ToString(), "");

                return result;
            }
        }
    }
}
