﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TestAPI20171114.Models;
using TestAPI20171114.Models.Request;

namespace TestAPI20171114.Controllers
{
    public class ManageListController : ApiController
    {
        [HttpPost]
        public ResultInfoT<object> Post()
        {
            var session = HttpContext.Current.Session;
            var request = HttpContext.Current.Request;
            var result = new ResultInfoT<object>();

            try
            {
                if (session["Login"] != null && session["Login"].ToString().TrimEnd() == "1")
                {
                    var managerId = int.Parse(session["ManagerId"].ToString());

                    using (var db = new livecloudEntities1())
                    {

                        var query = from m in db.dt_Manager
                                    join r in db.dt_ManagerRole
                                    on m.admin_role equals r.Id
                                    orderby m.id ascending
                                    select new
                                    {
                                        m.id,
                                        m.user_name,
                                        m.real_name,
                                        m.add_time,
                                        m.Status,
                                        r.RoleName
                                    };

                        var managerList = query.ToList().Select(o => new ManagerList()
                        {
                            ID = o.id,
                            UserName = o.user_name,
                            RealName = o.real_name,
                            AdminRole = o.RoleName,
                            AddTime = o.add_time.ToString("yyyy-MM-dd HH:mm:ss"),
                            Status = o.Status
                        }).ToList();

                        result.BackData = managerList;

                        result.Code = CodeConstantsHelper.RetGetDataSuc;
                        result.StrCode = CodeConstantsHelper.RetGetDataSucStr;
                        result.IsLogin = CodeConstantsHelper.RetIsLogin;
                    }
                }
                else
                {
                    result.Code = CodeConstantsHelper.RetParamErr;
                    result.StrCode = CodeConstantsHelper.UserIsNotLoginStr;
                    result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                }

                return result;
            }
            catch (Exception e)
            {
                result.Code = CodeConstantsHelper.RetParamErr;
                result.StrCode = "執行時出錯";
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                Log.Error("setPassword", "setPassword", e.ToString());

                return result;
            }
        }
    }
}
