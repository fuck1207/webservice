﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestAPI20171114.Models;
using System.Web;

namespace TestAPI20171114.Controllers
{
    public class getLotteryRecordController : ApiController
    {
        Models.livecloudEntities1 db = new Models.livecloudEntities1();
        [HttpPost]
        public ResultInfoT<object> Post()
        {
            ResultInfoT<object> result = new ResultInfoT<object>();
            HttpRequest staticContext = HttpContext.Current.Request;
            string Code = staticContext.Form["Code"] != null ? staticContext.Form["Code"] : "";
            DateTime StartTime = staticContext.Form["StartTime"] != null ? Convert.ToDateTime(staticContext.Form["StartTime"]) : System.DateTime.Now.Date;
            DateTime EndTime = staticContext.Form["EndTime"] != null ? Convert.ToDateTime(staticContext.Form["EndTime"]) : System.DateTime.Now.AddDays(1).Date;
            int PageSize = staticContext.Form["PageSize"] != null ? Convert.ToInt32(staticContext.Form["PageSize"]) : 10;
            int PageIndex = staticContext.Form["PageIndex"] != null ? Convert.ToInt32(staticContext.Form["PageIndex"]) : 0;

            try
            {
                if (HttpContext.Current.Session["Login"] != null && HttpContext.Current.Session["Login"].ToString().TrimEnd() == "1")
                {
                    int count = 0;
                    List<view_liveOpenRecord> dt_liveOpenRecord = (from s in db.dt_liveOpenRecord
                                                                   join livetable in db.dt_liveTable on s.tableId equals livetable.tableId
                                                                   join live in db.dt_live on s.liveId equals live.liveId
                                                                   join dealer in db.dt_dealer on s.dealerId equals dealer.dealerId
                                                                   where s.code == Code && s.startTime >= StartTime && s.endTime <= EndTime
                                                                   select new view_liveOpenRecord { ID = s.id, Code = s.code, Issue = s.issueNo, StartTime = s.startTime, EndTime = s.endTime, Name = dealer.dealerId + "-" + dealer.dealerName.TrimEnd(), Result = s.openNum != "---" ? s.openNum : "等待开奖", Time = s.add_time }).ToList();

                    count = dt_liveOpenRecord.Count();
                    dt_liveOpenRecord = dt_liveOpenRecord.OrderByDescending(o => o.Time).Skip(PageIndex * PageSize).Take(PageSize).ToList();

                    result.Code = CodeConstantsHelper.RetGetDataSuc;
                    result.BackData = dt_liveOpenRecord;
                    result.StrCode = CodeConstantsHelper.RetGetDataSucStr;
                    result.DataCount = count;
                    result.IsLogin = CodeConstantsHelper.RetIsLogin;

                    return result;

                }
                else
                {
                    result.Code = CodeConstantsHelper.RetParamErr;
                    result.StrCode = CodeConstantsHelper.UserIsNotLoginStr;
                    result.DataCount = 0;
                    result.IsLogin = CodeConstantsHelper.RetIsNoLogin;

                    return result;
                }
            }
            catch
            {
                result.Code = CodeConstantsHelper.RetParamErr;
                result.StrCode = CodeConstantsHelper.RetParamErrStr;
                result.DataCount = 0;
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;

                return result;
            }

          
        }
    }
}
