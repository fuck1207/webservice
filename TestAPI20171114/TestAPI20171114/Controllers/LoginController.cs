﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TestAPI20171114.Common;
using TestAPI20171114.Models;
using TestAPI20171114.Models.Request;

namespace TestAPI20171114.Controllers
{
    public class LoginController : ApiController
    {
        [HttpPost]
        public ResultInfoT<object> Post()
        {
            ResultInfoT<object> result = new ResultInfoT<object>();
            HttpRequest staticContext = HttpContext.Current.Request;
            string Password = "";
            string Username = "";
            string Action = "";
            if (!string.IsNullOrEmpty(staticContext.Form["Password"]))
                Password = staticContext.Form["Password"].ToString().TrimEnd();
            if (!string.IsNullOrEmpty(staticContext.Form["Username"]))
                Username = staticContext.Form["Username"].ToString().TrimEnd();
            if (!string.IsNullOrEmpty(staticContext.Form["Action"]))
                Action = staticContext.Form["Action"].ToString().TrimEnd();

            try
            {
                using (livecloudEntities1 db = new Models.livecloudEntities1())
                {
                    var manager = (from s in db.dt_Manager
                                      where s.user_name == Username && s.password == Password
                                      select s)
                                      .FirstOrDefault();

                    if (manager != null)
                    {
                        var role = (from r in db.dt_ManagerRole
                                    where r.Id == manager.admin_role
                                    select r).FirstOrDefault();

                        if (role != null)
                        {
                            var manageLog = new dt_ManageLog()
                            {
                                ManagerId = manager.id,
                                ManagerName = manager.user_name,
                                ActionType = "Login",
                                AddTime = DateTime.Now,
                                Remarks = "登入成功",
                                IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]
                            };
                            db.dt_ManageLog.Add(manageLog);
                            db.SaveChanges();

                            HttpContext.Current.Session["Login"] = "1";
                            HttpContext.Current.Session["ManagerId"] = manager.id.ToString();
                            HttpContext.Current.Session["ManagerName"] = manager.user_name;

                            result.Code = CodeConstantsHelper.RetGetDataSuc;
                            result.StrCode = "登录成功";
                            result.IsLogin = CodeConstantsHelper.RetIsLogin;
                            result.BackData = new GetRoleInfoBodyData()
                            {
                                barrageManage = role.BarrageManage.ToOnOff(),
                                blackList = role.BlackList.ToOnOff(),
                                dealerList = role.DealerList.ToOnOff(),
                                dealerManage = role.DealerManage.ToOnOff(),
                                dealerPost = role.DealerPost.ToOnOff(),
                                dealerTable = role.DealerTable.ToOnOff(),
                                dealerTime = role.DealerTime.ToOnOff(),
                                giftList = role.GiftList.ToOnOff(),
                                giftManage = role.GiftManage.ToOnOff(),
                                livecmsManage = role.LiveCmsManage.ToOnOff(),
                                liveManage = role.LiveManage.ToOnOff(),
                                manageLog = role.ManageLog.ToOnOff(),
                                Manager = role.Manager.ToOnOff(),
                                managerList = role.ManagerList.ToOnOff(),
                                manualReview = role.ManualReview.ToOnOff(),
                                roleManage = role.RoleManage.ToOnOff(),
                                safeWords = role.SafeWords.ToOnOff(),
                                specialWords = role.SpecialWords.ToOnOff(),
                                systemBarrage = role.SystemBarrage.ToOnOff(),
                                systemFace = role.SystemFace.ToOnOff(),
                                videoList = role.VideoList.ToOnOff(),
                                whiteList = role.WhiteList.ToOnOff(),
                                wordsManage = role.WordsManage.ToOnOff()
                            };
                        }
                        else
                        {
                            result.Code = CodeConstantsHelper.RetParamErr;
                            result.StrCode = "執行中出錯, 缺少權限數據";
                            result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                        }
                    }
                    else
                    {
                        result.Code = CodeConstantsHelper.RetParamErr;
                        result.StrCode = "帐户或密码错误";
                        result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                    }
                }
                Log.UseLog("dt_manager", Action, "Password" + Password + ";" + "Username:" + Username);
                return result;
            }
            catch (Exception ex)
            {
                result.Code = CodeConstantsHelper.RetParamErr;
                result.StrCode = "執行中出錯";
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                Log.Error("dt_manager", Action, "");
                return result;
            }


        }
    }
}
