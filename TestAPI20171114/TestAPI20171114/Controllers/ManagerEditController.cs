﻿using Microsoft.ApplicationInsights.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TestAPI20171114.Models;

namespace TestAPI20171114.Controllers
{
    public class ManagerEditController : ApiController
    {
        [HttpPost]
        public ResultInfoT<object> Post()
        {
            var session = HttpContext.Current.Session;
            var request = HttpContext.Current.Request;

            ResultInfoT<object> result = new ResultInfoT<object>();

            try
            {
                if (HttpContext.Current.Session["Login"] != null && HttpContext.Current.Session["Login"].ToString().TrimEnd() == "1")
                {

                    int id = int.TryParse(request.Form["ID"] ?? "-1", out id) ? id : -1;
                    string userName = request.Form["UserName"] ?? "";
                    string password = request.Form["Password"] ?? "";
                    string realName = request.Form["RealName"] ?? "";
                    int adminRole = int.TryParse(request.Form["AdminRole"] ?? "-1", out adminRole) ? adminRole : -1;
                    string type = request.Form["Type"] ?? "";

                    if (type == "add" && adminRole > 0 &&
                        !string.IsNullOrEmpty(userName) &&
                        !string.IsNullOrEmpty(password))
                    {
                        using (var db = new livecloudEntities1())
                        {
                            var role = (from r in db.dt_ManagerRole
                                        where r.Id == adminRole
                                        select r).FirstOrDefault();

                            var manager = (from m in db.dt_Manager
                                           where m.user_name.ToLower() == userName.ToLower()
                                           select m).FirstOrDefault();

                            if (role != null && manager == null)
                            {
                                manager = new dt_Manager()
                                {
                                    user_name = userName,
                                    password = password,
                                    add_time = DateTime.Now,
                                    admin_role = adminRole,
                                    real_name = (string.IsNullOrEmpty(realName))? realName : "",
                                    Status = 1
                                };

                                db.dt_Manager.Add(manager);

                                var manageLog = new dt_ManageLog()
                                {
                                    ManagerId = manager.id,
                                    ManagerName = session["ManagerName"].ToString(),
                                    ActionType = "ManagerEdit",
                                    AddTime = DateTime.Now,
                                    Remarks = "新增:" + userName + "為新帳戶",
                                    IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]
                                };
                                db.dt_ManageLog.Add(manageLog);
                                db.SaveChanges();

                                result.Code = CodeConstantsHelper.RetGetDataSuc;
                                result.StrCode = CodeConstantsHelper.RetGetDataSucStr;
                                result.IsLogin = CodeConstantsHelper.RetIsLogin;
                            }
                            else
                            {
                                result.Code = CodeConstantsHelper.RetParamErr;
                                result.StrCode = "沒有此角色或用戶名重複";
                                result.IsLogin = CodeConstantsHelper.RetIsLogin;
                            }
                        }
                    }
                    else if (type == "edit" && id > 0)
                    {
                        using (var db = new livecloudEntities1())
                        {
                            var manager = (from m in db.dt_Manager
                                           where m.id == id
                                           select m).FirstOrDefault();

                            if (manager != null)
                            {
                                manager.password = (!string.IsNullOrEmpty(password))
                                    ? password
                                    : manager.password;
                                manager.real_name = (!string.IsNullOrEmpty(realName))
                                    ? realName
                                    : manager.real_name;

                                if (adminRole > 0 && (from r in db.dt_ManagerRole
                                                      where r.Id == adminRole
                                                      select r).FirstOrDefault() != null)
                                {
                                    manager.admin_role = adminRole;
                                }

                                var manageLog = new dt_ManageLog()
                                {
                                    ManagerId = manager.id,
                                    ManagerName = session["ManagerName"].ToString(),
                                    ActionType = "ManagerEdit",
                                    AddTime = DateTime.Now,
                                    Remarks = "修改帳戶:" + manager.id + "資料",
                                    IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]
                                };
                                db.dt_ManageLog.Add(manageLog);
                                db.SaveChanges();

                                result.Code = CodeConstantsHelper.RetGetDataSuc;
                                result.StrCode = CodeConstantsHelper.RetGetDataSucStr;
                                result.IsLogin = CodeConstantsHelper.RetIsLogin;
                            }
                            else
                            {
                                result.Code = CodeConstantsHelper.RetParamErr;
                                result.StrCode = "沒有此用戶";
                                result.IsLogin = CodeConstantsHelper.RetIsLogin;
                            }
                        }
                    }
                    else
                    {
                        result.Code = CodeConstantsHelper.RetParamErr;
                        result.StrCode = "參數錯誤";
                        result.IsLogin = CodeConstantsHelper.RetIsLogin;
                    }
                }
                else
                {
                    result.Code = CodeConstantsHelper.RetParamErr;
                    result.StrCode = CodeConstantsHelper.UserIsNotLoginStr;
                    result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                    Log.UseLog("ManageEdit", "ManagerEdit", "未登入");
                }
            }
            catch (Exception e)
            {
                result.Code = CodeConstantsHelper.RetParamErr;
                result.StrCode = "執行時出錯";
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                Log.Error("ManageEdit", "ManagerEdit", "");
            }
            return result;
        }
    }
}
