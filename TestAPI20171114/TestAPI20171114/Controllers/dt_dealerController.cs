﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.SessionState;
using TestAPI20171114.Models;
using System.Web;
using System.Reflection;
using Newtonsoft.Json;
using System.IO;

namespace TestAPI20171114.Controllers
{
    public static class FromConverter
    {
        public static T ToModel<T>(this System.Collections.Specialized.NameValueCollection nvc, bool NullCheck = true) where T : class, new()
        {

            bool flog = (NullCheck && nvc != null && nvc.Keys.Count > 0);

            if (flog)
            {
                var model = new T();
                PropertyInfo[] propertyInfos = model.GetType().GetProperties();


                foreach (PropertyInfo propertyInfo in propertyInfos)
                {
                    var value = nvc[propertyInfo.Name];
                    if (value == null)
                        continue;

                    propertyInfo.SetValue(model, Convert.ChangeType(value, propertyInfo.PropertyType), null);
                }

                return model;
            }

            return new T();
        }


    }

    public class dt_dealerController : ApiController
    {
        [HttpGet]
        public string Get(int id)
        {
            return id.ToString();
        }
        Models.livecloudEntities1 db = new livecloudEntities1();

        //dealer add
        [HttpPost]
        public ResultInfoT<object> Post()
        {
            //var ss = System.Web.HttpContext.Current.Request.Form.ToModel<form_dealer>();

            //if (ss.IsVerify && ss.isLogin)
            //{

            //}
            ResultInfoT<object> result = new ResultInfoT<object>();
            ModelHelper mh = new ModelHelper();
            HttpRequest staticContext = HttpContext.Current.Request;
            string Action = staticContext.Form["Action"].ToString().TrimEnd();
            //ss.ToModel<form_dealer>();
            try
            {
              
               
                Dictionary<string, string> dicFrom = new Dictionary<string, string>();
                form_dealer form_dealer = new form_dealer();
                dt_dealer dt_dealer = new dt_dealer();
                List<form_dealer> listform_dealer = new List<form_dealer>();
                listform_dealer.Add(form_dealer);
                


                result.Code = -1;
                result.StrCode = "错误信息";
                result.IsLogin = false;
                if (HttpContext.Current.Session["Login"] != null && HttpContext.Current.Session["Login"].ToString().TrimEnd() == "1")
                {
                    result.IsLogin = true;
                    //write(Action, "Session" + HttpContext.Current.Session["Login"].ToString() + ";");
                    if (Action == "setDealer")
                    {
                        string Type = staticContext.Form["Type"].ToString().TrimEnd();
                        //Form轉Dic
                        dicFrom = mh.ConvertFormToDic(staticContext);
                        //Dic轉Model
                        mh.ConvertDicToModel(dicFrom, form_dealer);
                        //繫節EF
                        mh.MappingEF<form_dealer>(listform_dealer, dt_dealer);

                        dt_dealer.update_time = System.DateTime.Now;
                        if (Type == "add")
                        {
                            dt_dealer.add_time = System.DateTime.Now;
                            db.dt_dealer.Add(dt_dealer);
                            if (!ModelState.IsValid)
                            {
                                BadRequest(ModelState);
                                return result;
                            }

                            db.SaveChanges();
                            result.Code = 1;
                            result.StrCode = "成功信息";
                            return result;
                            //result.BackData = dt_dealer;
                        }
                        else if (Type == "edit")
                        {
                            db.Configuration.ValidateOnSaveEnabled = false;
                            int ID = Convert.ToInt32(staticContext.Form["ID"].ToString());
                            using (db)
                            {
                                var dt_dealer2 = (from s in db.dt_dealer where s.id == ID select s).FirstOrDefault();
                                if (dt_dealer2 != null)
                                {
                                    dt_dealer.add_time = dt_dealer2.add_time;
                                    db.Entry(dt_dealer2).CurrentValues.SetValues(dt_dealer);
                                    db.SaveChanges();
                                    result.Code = 1;
                                    result.StrCode = "成功信息";
                                    // result.BackData = dt_dealer;

                                }
                            }
                            return result;
                        }
                        else
                        {
                            return result;
                        }
                    }
                    else if (Action == "getDealer")
                    {
                        DateTime time_star = staticContext.Form["StartTime"] != null ? Convert.ToDateTime(staticContext.Form["StartTime"]) : System.DateTime.Now;
                        DateTime time_end = staticContext.Form["EndTime"] != null ? Convert.ToDateTime(staticContext.Form["EndTime"]).AddDays(1) : System.DateTime.Now;
                        string dealer_id = staticContext.Form["Code"] != null ? staticContext.Form["Code"].TrimEnd() : "";
                        string dealer_name = staticContext.Form["Name"] != null ? staticContext.Form["Name"].TrimEnd() : "";

                        List<dt_dealer> dealer_view = (from s in db.dt_dealer where s.add_time >= time_star && s.add_time <= time_end select s).ToList();

                        if (dealer_id != "")
                            dealer_view = dealer_view.Where(o => o.dealerId == dealer_id).ToList();
                        if (dealer_name != "")
                            dealer_view = dealer_view.Where(o => o.dealerName.TrimEnd() == dealer_name).ToList();

                        List<Dictionary<string, object>> listdic = new List<Dictionary<string, object>>();
                        //Model轉dic
                        mh.ConvertModelToDic<dt_dealer>(dealer_view, listdic);
                        view_dealer Model = new view_dealer();
                        mh.ConvertDicToModel<view_dealer>(listdic, Model);

                        result.BackData = dealer_view;
                        result.Code = 1;
                        result.StrCode = "成功信息";
                        result.DataCount = dealer_view.Count();
                  
                        return result;

                    }
                }
                else
                {
                    result.StrCode = "無此session错误信息";
                }
            }
            catch (Exception e )
            {
                result.StrCode = "错误信息";
                Log.Error("dt_dealer", Action,e.ToString());
            }
            



            return result;

        }

       
    }
}