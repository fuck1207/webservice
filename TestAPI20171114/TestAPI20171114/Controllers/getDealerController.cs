﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TestAPI20171114.Models;

namespace TestAPI20171114.Controllers
{
    public class getDealerController : ApiController
    {
        Models.livecloudEntities1 db = new livecloudEntities1();

        [HttpPost]
        public ResultInfoT<object> Post()
        {
            ResultInfoT<object> result = new ResultInfoT<object>();
            HttpRequest staticContext = HttpContext.Current.Request;
            DateTime time_star = staticContext.Form["StartTime"] != null ? Convert.ToDateTime(staticContext.Form["StartTime"]) : System.DateTime.Now;
            DateTime time_end = staticContext.Form["EndTime"] != null ? Convert.ToDateTime(staticContext.Form["EndTime"]).AddDays(1) : System.DateTime.Now;
            string dealer_id = staticContext.Form["Code"] != null ? staticContext.Form["Code"].TrimEnd() : "";
            string dealer_name = staticContext.Form["Name"] != null ? staticContext.Form["Name"].TrimEnd() : "";
            int index = staticContext.Form["PageIndex"] != null ? Convert.ToInt32(staticContext.Form["PageIndex"].TrimEnd()) : 0;
            int size = staticContext.Form["PageSize"] != null ? Convert.ToInt32(staticContext.Form["PageSize"].TrimEnd()) : 0;

            string Action = staticContext.Form["Action"].ToString().TrimEnd();

            try
            {
                if (HttpContext.Current.Session["Login"] != null && HttpContext.Current.Session["Login"].ToString().TrimEnd() == "1")
                {
                    List<view_dealer> dealer_view = (from s in db.dt_dealer where s.add_time >= time_star && s.add_time <= time_end select new view_dealer { ID = s.id, Code = s.dealerId ,Name = s.dealerName.TrimEnd(),Sex = s.sex,Age = s.age,City = s.area.TrimEnd(),Height = s.height,BWH = s.bwh.TrimEnd(), Weight = s.weight ,Image = s.img.TrimEnd(),Photo = s.img2.TrimEnd(), AddTime  = s.add_time}).ToList();
                    int count = dealer_view.Count();
                    dealer_view = dealer_view.OrderByDescending(O => O.ID).Skip(index * size).Take(size).ToList();

                   // List<dt_dealer> dealer_view = (from s in db.dt_dealer where s.add_time >= time_star && s.add_time <= time_end select s ).ToList();

                    if (dealer_id != "")
                        dealer_view = dealer_view.Where(o => o.Code == dealer_id).ToList();
                    if (dealer_name != "")
                        dealer_view = dealer_view.Where(o => o.Name.TrimEnd() == dealer_name).ToList();

                    //List<Dictionary<string, object>> listdic = new List<Dictionary<string, object>>();
                    ////Model轉dic
                    //mh.ConvertModelToDic<dt_dealer>(dealer_view, listdic);
                    //view_dealer Model = new view_dealer();
                    //mh.ConvertDicToModel<view_dealer>(listdic, Model);

                    result.BackData = dealer_view;
                    result.Code = CodeConstantsHelper.RetGetDataSuc;
                    result.StrCode = "成功信息";
                    result.DataCount = count;
                    result.IsLogin = CodeConstantsHelper.RetIsLogin;


                    return result;
                }
                else
                {
                    result.Code = CodeConstantsHelper.RetParamErr;
                    result.StrCode = CodeConstantsHelper.UserIsNotLoginStr;
                    result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                    Log.UseLog("dt_dealer", Action, "未登入");
                    return result;
                }
            }
            catch(Exception E)
            {
                result.Code = CodeConstantsHelper.RetParamErr;
                result.StrCode = "错误信息";
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                return result;
            }





        }

    }
}
