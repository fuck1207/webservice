﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestAPI20171114.Models;
using System.Web;

namespace TestAPI20171114.Controllers
{
    public class setLiveSwitchController : ApiController
    {
        Models.livecloudEntities1 db = new Models.livecloudEntities1();
        [HttpPost]
        public ResultInfoT<object> Post()
        {
            ResultInfoT<object> result = new ResultInfoT<object>();
            HttpRequest staticContext = HttpContext.Current.Request;
            int ID = staticContext.Form["ID"] != null ? Convert.ToInt32(staticContext.Form["ID"]) : 0;
            string Type = staticContext.Form["Type"] != null ? staticContext.Form["Type"] : "";
            string Time = staticContext.Form["Time"] != null ? staticContext.Form["Time"] : "";
            string Action = staticContext.Form["Action"] != null ? staticContext.Form["Action"] : "";

            try
            {
                if (HttpContext.Current.Session["Login"] != null && HttpContext.Current.Session["Login"].ToString().TrimEnd() == "1")
                {
                    var dt_liveList = (from s in db.dt_liveList where s.id == ID select s).FirstOrDefault();
                    if(dt_liveList != null)
                    {
                        dt_liveList.state = Type == "true" ? Convert.ToByte(1) : Convert.ToByte(0);
                        dt_liveList.stop_time = Time;
                        dt_liveList.update_time = System.DateTime.Now.Date;
                        db.SaveChanges();

                        result.IsLogin = CodeConstantsHelper.RetIsLogin;
                        result.Code = CodeConstantsHelper.RetGetDataSuc;
                        result.StrCode = CodeConstantsHelper.RetGetDataSucStr;
                        Log.UseLog("dt_LiveList", Action, "");
                     
                    }
                }
                else
                {
                    result.Code = CodeConstantsHelper.RetParamErr;
                    result.StrCode = CodeConstantsHelper.UserIsNotLoginStr;
                    result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                    Log.UseLog("dt_LiveList", Action, "未登入");

                }
                return result;
            }
            catch(Exception e)
            {
                result.Code = CodeConstantsHelper.RetParamErr;
                result.StrCode = CodeConstantsHelper.RetParamErrStr;
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                Log.Error("dt_LiveList", Action, e.ToString());

                return result;
            }
            


            
        }
    }
}
