﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TestAPI20171114.Models;

namespace TestAPI20171114.Controllers
{
    public class setPasswordController : ApiController
    {
        [HttpPost]
        public ResultInfoT<object> Post()
        {
            var session = HttpContext.Current.Session;
            var request = HttpContext.Current.Request;
            var result = new ResultInfoT<object>();

            try
            {
                if (session["Login"] != null && session["Login"].ToString().TrimEnd() == "1")
                {
                    var managerId = int.Parse(session["ManagerId"].ToString());

                    var userName = request.Form["UserName"] ?? "";
                    var oldPassword = request.Form["oldPassword"] ?? "";
                    var newPassword = request.Form["newPassword"] ?? "";
                    var type = request.Form["Type"] ?? "";

                    // 檢查參數
                    if (string.IsNullOrEmpty(userName) ||
                        string.IsNullOrEmpty(oldPassword) ||
                        string.IsNullOrEmpty(newPassword) ||
                        string.IsNullOrEmpty(type))
                    {
                        result.Code = CodeConstantsHelper.RetParamErr;
                        result.StrCode = CodeConstantsHelper.RetParamErrStr;
                        result.IsLogin = CodeConstantsHelper.RetIsLogin;
                    }
                    else if (type.ToLower() == "edit")
                    {
                        using (var db = new livecloudEntities1())
                        {
                            var query = from m in db.dt_Manager
                                        where m.user_name == userName && m.password == oldPassword
                                        select m;

                            var manager = query.FirstOrDefault();

                            if (manager != null)
                            {
                                manager.password = newPassword;

                                var s = db.SaveChanges();

                                Log.Error("setPassword", "db.savechange", s.ToString());

                                result.IsLogin = CodeConstantsHelper.RetIsLogin;
                                result.Code = CodeConstantsHelper.RetGetDataSuc;
                                result.StrCode = CodeConstantsHelper.RetGetDataSucStr;
                            }
                            else
                            {
                                // 用戶名不對 or 舊密碼不對
                                result.Code = CodeConstantsHelper.RetParamErr;
                                result.StrCode = "用戶名不對 or 舊密碼不對";
                                result.IsLogin = CodeConstantsHelper.RetIsLogin;
                            }
                        }
                    }
                    else
                    {
                        result.Code = CodeConstantsHelper.RetParamErr;
                        result.StrCode = "type 沒有對應動作";
                        result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                    }

                }
                else
                {
                    result.Code = CodeConstantsHelper.RetParamErr;
                    result.StrCode = CodeConstantsHelper.UserIsNotLoginStr;
                    result.IsLogin = CodeConstantsHelper.RetIsNoLogin;

                }
                return result;
            }
            catch (Exception e)
            {
                result.Code = CodeConstantsHelper.RetParamErr;
                result.StrCode = "執行時出錯";
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                Log.Error("setPassword", "setPassword", e.ToString());

                return result;
            }
        }
    }
}
