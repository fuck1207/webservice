﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;

namespace TestAPI20171114.Controllers
{
    public class ModelHelper
    {
        //字典轉Model
        public void ConvertDicToModel(Dictionary<string, string> dic_source, object Model)
        {
            try
            {
                Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();
                PropertyInfo[] propertyInfos = Model.GetType().GetProperties();
                foreach (PropertyInfo propertyInfo in propertyInfos)
                {
                    if (!dicPropertyInfo.ContainsKey(propertyInfo.Name))
                    {
                        dicPropertyInfo.Add(propertyInfo.Name, propertyInfo);
                    }
                }

                foreach (var dic in dic_source)
                {
                    string key = dic.Key.ToString().TrimEnd();
                    string value = dic.Value.ToString().TrimEnd();
                    if (!dicPropertyInfo.ContainsKey(key))
                    {
                        foreach (var dicpro in dicPropertyInfo)
                        {
                            foreach (var ss in dicPropertyInfo[dicpro.Key].CustomAttributes)
                            {
                                foreach (var ss2 in ss.ConstructorArguments)
                                {
                                    if (key == ss.ConstructorArguments[0].Value.ToString().TrimEnd())
                                    {
                                        object bjValue2 = Convert.ChangeType(value, dicPropertyInfo[dicpro.Key].PropertyType);
                                        dicPropertyInfo[dicpro.Key].SetValue(Model, bjValue2, null);
                                        break;
                                    }
                                }

                            }

                        }

                        continue;
                    }
                    object bjValue = Convert.ChangeType(value, dicPropertyInfo[key].PropertyType);
                    dicPropertyInfo[key].SetValue(Model, bjValue, null);
                }
            }
            catch (Exception e)
            {

            }


        }
        public List<T> ConvertDicToModel<T>(List<Dictionary<string, object>> dic_source, T Model)  where T : new()
        {
            try
            {
                List<T> ListModel = new List<T>();
                
                Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();

                PropertyInfo[] propertyInfos = Model.GetType().GetProperties();
                foreach (PropertyInfo propertyInfo in propertyInfos)
                {
                    foreach (var sin2 in propertyInfo.CustomAttributes)
                    {
                        if (!dicPropertyInfo.ContainsKey(sin2.ConstructorArguments[0].Value.ToString()))
                        {
                            dicPropertyInfo.Add(sin2.ConstructorArguments[0].Value.ToString(), propertyInfo);
                        }
                    }

                }

                foreach (var dic in dic_source)
                {
                    var MM = new T();
                    foreach (var key in dic)
                    {
                        string key_v = key.Key.ToString().TrimEnd();
                        string value = key.Value.ToString().TrimEnd();
                        if (!dicPropertyInfo.ContainsKey(key_v))
                        {
                            continue;
                        }
                        else
                        {
                            object bjValue = Convert.ChangeType(value, dicPropertyInfo[key_v].PropertyType);
                            dicPropertyInfo[key_v].SetValue(MM, bjValue, null);
                        }
                  
                    }
                    ListModel.Add(MM);

                }
                return ListModel;



            }
            catch (Exception e)
            {
                return null;
            }


        }
        //HttpRequest轉字典
        public Dictionary<string, string> ConvertFormToDic(HttpRequest staticContext)
        {
            Dictionary<string, string> dicFrom = new Dictionary<string, string>();
            for (int i = 0; i < staticContext.Form.AllKeys.Count(); i++)
            {
                string key = staticContext.Form.AllKeys[i].ToString().TrimEnd();
                string value = staticContext.Form[i].ToString().TrimEnd();
                if (!dicFrom.ContainsKey(key))
                {
                    dicFrom.Add(key, value);
                }
            }
            return dicFrom;
        }

        //自訂繫節
        public void MappingEF<T>(List<T> Model, object newModel)
        {
            foreach (var s in newModel.GetType().GetProperties())
            {
                foreach (var sin in Model)
                {

                    foreach (var sin2 in sin.GetType().GetProperties())
                    {
                        if (s.Name == sin2.Name)
                        {
                            var kk = sin2.GetValue(sin, null) ;
                            object bjValue = Convert.ChangeType(kk, sin2.PropertyType);
                            //s.GetValue(newModel, null);
                            s.SetValue(newModel, bjValue, null);
                            //object bjValue = Convert.ChangeType(value, dicPropertyInfo[key].PropertyType);
                            //dicPropertyInfo[key].SetValue(Model, bjValue, null);
                        }
                    }

                }
            }

        }

        //自訂繫節輸出
        public void MappingOut<T>(List<T> Model, object newModel)
        {
            List<object> list = new List<object>();
            PropertyInfo[] propertyInfos = Model.GetType().GetProperties();
            foreach (var sin in Model)
            {

                foreach (var sin2 in sin.GetType().GetProperties())
                {
                    var kk = sin2.GetValue(sin, null);
                    object mo = new object();

                    // mo.GetType().GetProperties().SetValue(newModel, kk, null);

                }

            }


        }


        //Model轉字典
        public void ConvertModelToDic<T>(List<T> Model, List<Dictionary<string, object>> Listdic_source)
        {
            try
            {

                foreach (var Tmodel in Model)
                {
                    Dictionary<string, object> dic_source = new Dictionary<string, object>();
                    foreach (var propertyInfo in Tmodel.GetType().GetProperties())
                    {
                        if (!dic_source.ContainsKey(propertyInfo.Name))
                        {
                            var kk = propertyInfo.GetValue(Tmodel, null);
                            dic_source.Add(propertyInfo.Name, kk);
                        }
                    }
                    Listdic_source.Add(dic_source);
                }



            }
            catch (Exception e)
            {


            }


        }



        //public void ToModel<T>(System.Collections.Generic.KeyValuePair<string,object> kvp)
        //{
        //    Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();
        //    PropertyInfo[] propertyInfos = Model.GetType().GetProperties();
        //    foreach (PropertyInfo propertyInfo in propertyInfos)
        //    {
        //        if (!dicPropertyInfo.ContainsKey(propertyInfo.Name))
        //        {
        //            dicPropertyInfo.Add(propertyInfo.Name, propertyInfo);
        //        }
        //    }
        //    foreach (var dic in dic_source)
        //    {
        //        string key = dic.Key.ToString().TrimEnd();
        //        string value = dic.Value.ToString().TrimEnd();
        //        if (!dicPropertyInfo.ContainsKey(key))
        //        {
        //            continue;
        //        }
        //        object bjValue = Convert.ChangeType(value, dicPropertyInfo[key].PropertyType);
        //        dicPropertyInfo[key].SetValue(Model, bjValue, null);
        //    }

        //}
    }
}