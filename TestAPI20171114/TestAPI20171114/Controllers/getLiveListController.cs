﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TestAPI20171114.Models;

namespace TestAPI20171114.Controllers
{
    public class getLiveListController : ApiController
    {
        Models.livecloudEntities1 db = new Models.livecloudEntities1();
        [HttpPost]
        public ResultInfoT<object> Post()
        {
            ResultInfoT<object> result = new ResultInfoT<object>();

            HttpRequest staticContext = HttpContext.Current.Request;
            string Action = staticContext.Form["Action"].ToString().TrimEnd();
            view_liveList getLiveList = new view_liveList();
            try
            {
                Dictionary<string, string> dicFrom = new Dictionary<string, string>();
                if (HttpContext.Current.Session["Login"] != null && HttpContext.Current.Session["Login"].ToString().TrimEnd() == "1")
                {
                    //var liveList_view = db.dt_liveList.Join(db.dt_liveTable, s => s.tableId, sm => sm.tableId, (s, sm) => new { dealer = s.dealerId, tableName = sm.tableName }).ToList();
                    List<view_liveList> liveList_view = (from s in db.dt_liveList
                                                         join liveTable in db.dt_liveTable on s.tableId equals liveTable.tableId
                                                         join dealer in db.dt_dealer on s.dealerId equals dealer.dealerId
                                                         join live in db.dt_live on s.liveId equals live.liveId
                                                         select new view_liveList { ID = s.id, Code = s.code, LiveName = liveTable.tableName, Title = live.liveName, Name = dealer.dealerName.TrimEnd(), State = s.state == 1 & s.stop_time == "" ? "运行中" : "停运" + s.stop_time, Time = s.add_time }).OrderByDescending(o => o.ID).ToList();

                    result.BackData = liveList_view;
                    result.Code = CodeConstantsHelper.RetGetDataSuc;
                    result.StrCode = CodeConstantsHelper.RetGetDataSucStr;
                    result.IsLogin = CodeConstantsHelper.RetIsLogin;
                    Log.UseLog("dt_LiveList", Action, "");

                    return result;


                }
                else
                {
                    result.Code = CodeConstantsHelper.RetParamErr;
                    result.StrCode = CodeConstantsHelper.UserIsNotLoginStr;
                    result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                    Log.UseLog("dt_LiveList", Action, "未登入");

                    return result;
                }
            }
            catch (Exception e)
            {
                result.Code = CodeConstantsHelper.RetParamErr;
                result.StrCode = CodeConstantsHelper.RetParamErrStr;
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                Log.Error("dt_LiveList", Action, e.ToString());

                return result;
            }

      
        }
    }
}
