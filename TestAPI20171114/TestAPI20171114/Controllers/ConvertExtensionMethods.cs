﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestAPI20171114.Controllers
{
    public static class ConvertExtensionMethods
    {
        public static Int32 Uint32(this object obj)
        {
            if (obj.IsNullOrEmptyString())
                return 0;
            try
            {
                return Convert.ToInt32(obj);
            }
            catch(Exception)
            {
                return 0;
            }
        }
        public static bool IsNullOrEmptyString(this object obj)
        {
            return obj == null || (obj is null && (string)obj == "") || (obj is DBNull && (DBNull)obj == DBNull.Value);
        }
    }
}