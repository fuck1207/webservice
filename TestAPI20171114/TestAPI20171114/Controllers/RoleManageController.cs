﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TestAPI20171114.Models;

namespace TestAPI20171114.Controllers
{
    public class RoleManageController : ApiController
    {
        Models.livecloudEntities1 db = new livecloudEntities1();
        [HttpPost]
        public ResultInfoT<object> Post()
        {
            ResultInfoT<object> result = new ResultInfoT<object>();
            HttpRequest staticContext = HttpContext.Current.Request;
            string Action = staticContext.Form["Action"].ToString().TrimEnd();
            view_liveList getLiveList = new view_liveList();
            try
            {
                if (HttpContext.Current.Session["Login"] != null && HttpContext.Current.Session["Login"].ToString().TrimEnd() == "1")
                {
                    List<view_role> managerRole_view = (from g in db.dt_ManagerRole select new view_role { ID = g.Id, Name = g.RoleName, Time = g.AddTime }).ToList();
                    result.BackData = managerRole_view;
                    result.Code = CodeConstantsHelper.RetGetDataSuc;
                    result.StrCode = "成功信息";
                    result.DataCount = managerRole_view.Count();
                    result.IsLogin = CodeConstantsHelper.RetIsLogin;
                    return result;
                }
                else
                {
                    result.Code = CodeConstantsHelper.RetParamErr;
                    result.StrCode = CodeConstantsHelper.UserIsNotLoginStr;
                    result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                    Log.UseLog("dt_roleManage", Action, "未登入");
                    return result;
                }

            }
            catch (Exception e)
            {
                result.StrCode = "错误信息";
                Log.Error("dt_roleManage", Action, e.ToString());
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                return result;
            }
        }
    }
}
