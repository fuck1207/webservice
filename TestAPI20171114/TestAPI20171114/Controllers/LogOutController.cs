﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TestAPI20171114.Models;

namespace TestAPI20171114.Controllers
{
    public class LogOutController : ApiController
    {
        Models.livecloudEntities1 db = new Models.livecloudEntities1();


        [HttpPost]
        public ResultInfoT<object> Post()
        {
            ResultInfoT<object> result = new ResultInfoT<object>();
           

            HttpRequest staticContext = HttpContext.Current.Request;
            string Action = "";
            
            if (!string.IsNullOrEmpty(staticContext.Form["Action"]))
                Action = staticContext.Form["Action"].ToString().TrimEnd();

            try
            {
                result.Code = CodeConstantsHelper.RetGetDataSuc;
                result.StrCode = "退出成功";
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                HttpContext.Current.Session["Login"] = "0";
                HttpContext.Current.Session["MandgerId"] = "-1";
                Log.UseLog("dt_manager", Action, "");
                return result;
            }
            catch
            {
                result.Code = CodeConstantsHelper.RetParamErr;
                result.StrCode = "登录失敗";
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                Log.Error("dt_manager", Action, "");
                return result;
            }


        }
    }
}
