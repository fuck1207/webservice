﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TestAPI20171114.Models;

namespace TestAPI20171114.Controllers
{
    public class ManageisLockController : ApiController
    {
        [HttpPost]
        public ResultInfoT<object> Post()
        {
            var session = HttpContext.Current.Session;
            var request = HttpContext.Current.Request;
            var result = new ResultInfoT<object>();

            try
            {
                if (session["Login"] != null && session["Login"].ToString().TrimEnd() == "1")
                {
                    var managerId = int.Parse(session["ManagerId"].ToString());

                    int id = int.TryParse(request.Form["ID"] ?? "-1", out id) ? id : -1;
                    byte status = byte.TryParse(request.Form["Status"] ?? "-1", out status) ? status : (byte)100;

                    if (id <= 0 || status < 0 || status > 1)
                    {
                        result.Code = CodeConstantsHelper.RetParamErr;
                        result.StrCode = "參數錯誤";
                        result.IsLogin = CodeConstantsHelper.RetIsLogin;
                    }
                    else
                    {
                        using (var db = new livecloudEntities1())
                        {
                            var manager = db.dt_Manager.Where(o => o.id == id).FirstOrDefault();

                            if (manager != null)
                            {
                                manager.Status = status;

                                db.SaveChanges();

                                result.Code = CodeConstantsHelper.RetGetDataSuc;
                                result.StrCode = CodeConstantsHelper.RetGetDataSucStr;
                                result.IsLogin = CodeConstantsHelper.RetIsLogin;
                            }
                            else
                            {
                                result.Code = CodeConstantsHelper.RetParamErr;
                                result.StrCode = "找不到ID:" + id + "的Manager";
                                result.IsLogin = CodeConstantsHelper.RetIsLogin;
                            }
                        }
                    }

                }
                else
                {
                    result.Code = CodeConstantsHelper.RetParamErr;
                    result.StrCode = CodeConstantsHelper.UserIsNotLoginStr;
                    result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                }
                return result;
            }
            catch (Exception e)
            {
                result.Code = CodeConstantsHelper.RetParamErr;
                result.StrCode = "執行時出錯";
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                Log.Error("setPassword", "setPassword", e.ToString());

                return result;
            }
        }
    }
}
