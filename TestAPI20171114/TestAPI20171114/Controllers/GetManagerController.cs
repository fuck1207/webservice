﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TestAPI20171114.Models;
using TestAPI20171114.Models.Request;

namespace TestAPI20171114.Controllers
{
    public class GetManagerController : ApiController
    {
        [HttpPost]
        public ResultInfoT<object> Post()
        {
            var session = HttpContext.Current.Session;
            var request = HttpContext.Current.Request;
            var result = new ResultInfoT<object>();

            try
            {
                if (session["Login"] != null && session["Login"].ToString().TrimEnd() == "1")
                {
                    var managerId = int.Parse(session["ManagerId"].ToString());

                    int id = int.TryParse(request.Form["ID"] ?? "-1", out id) ? id : -1;

                    // 檢查參數
                    if (id <= 0)
                    {
                        result.Code = CodeConstantsHelper.RetParamErr;
                        result.StrCode = "參數錯誤";
                        result.IsLogin = CodeConstantsHelper.RetIsLogin;
                    }
                    else
                    {
                        using (var db = new livecloudEntities1())
                        {
                            var query = from m in db.dt_Manager
                                        where m.id == id
                                        select m;

                            var manager = query.FirstOrDefault();

                            if (manager != null)
                            {
                                result.BackData = new GetManager()
                                {
                                    ID = manager.id,
                                    UserName = manager.user_name,
                                    RealName = manager.real_name,
                                    AdminRoleID = manager.admin_role
                                };

                                result.IsLogin = CodeConstantsHelper.RetIsLogin;
                                result.Code = CodeConstantsHelper.RetGetDataSuc;
                                result.StrCode = CodeConstantsHelper.RetGetDataSucStr;
                            }
                            else
                            {
                                result.Code = CodeConstantsHelper.RetParamErr;
                                result.StrCode = "找不到ID:" + id + "用戶";
                                result.IsLogin = CodeConstantsHelper.RetIsLogin;
                            }
                        }
                    }
                }
                else
                {
                    result.Code = CodeConstantsHelper.RetParamErr;
                    result.StrCode = CodeConstantsHelper.UserIsNotLoginStr;
                    result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                }

                return result;
            }
            catch (Exception e)
            {
                result.Code = CodeConstantsHelper.RetParamErr;
                result.StrCode = "執行時出錯";
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                Log.Error("setPassword", "setPassword", e.ToString());

                return result;
            }
        }
    }
}
