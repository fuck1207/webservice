﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TestAPI20171114.Models;

namespace TestAPI20171114.Controllers
{
    public class getDealerIDController : ApiController
    {
        Models.livecloudEntities1 db = new livecloudEntities1();

        [HttpPost]
        public ResultInfoT<object> Post()
        {
            ResultInfoT<object> result = new ResultInfoT<object>();
            HttpRequest staticContext = HttpContext.Current.Request;
            int ID = staticContext.Form["ID"] != null ? Convert.ToInt32(staticContext.Form["ID"].TrimEnd()) : 0;

            string Action = staticContext.Form["Action"].ToString().TrimEnd();

            try
            {
                if (HttpContext.Current.Session["Login"] != null && HttpContext.Current.Session["Login"].ToString().TrimEnd() == "1")
                {
                    List<view_dealer> dealer_view = (from s in db.dt_dealer where s.id == ID select new view_dealer { ID = s.id, Code = s.dealerId, Name = s.dealerName.TrimEnd(), Sex = s.sex, Age = s.age, City = s.area.TrimEnd(), Height = s.height, BWH = s.bwh.TrimEnd(), Weight = s.weight, Image = s.img.TrimEnd(), Photo = s.img2.TrimEnd(), AddTime = s.add_time }).ToList();
                    
                    result.BackData = dealer_view;
                    result.Code = CodeConstantsHelper.RetGetDataSuc;
                    result.StrCode = "成功信息";
                    result.DataCount = dealer_view.Count();
                    result.IsLogin = CodeConstantsHelper.RetIsLogin;
                    return result;
                }
                else
                {
                    result.Code = CodeConstantsHelper.RetParamErr;
                    result.StrCode = CodeConstantsHelper.UserIsNotLoginStr;
                    result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                    Log.UseLog("dt_dealer", Action, "未登入");
                    return result;
                }
            }
            catch(Exception e)
            {
                result.Code = CodeConstantsHelper.RetParamErr;
                result.StrCode = "错误信息";
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                return result;
            }





        }
    }
}
