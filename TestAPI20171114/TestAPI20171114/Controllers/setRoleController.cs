﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using TestAPI20171114.Models;
using TestAPI20171114.Common;
using TestAPI20171114.Models.Request;

namespace TestAPI20171114.Controllers
{
    public class setRoleController : ApiController
    {
        [HttpPost]
        public ResultInfoT<object> Post()
        {
            var session = HttpContext.Current.Session;
            var request = HttpContext.Current.Request;
            var result = new ResultInfoT<object>();

            try
            {
                if (session["Login"] != null && session["Login"].ToString().TrimEnd() == "1")
                {
                    var managerId = int.Parse(session["ManagerId"].ToString());

                    int id = int.TryParse(request.Form["ID"] ?? "-1", out id) ? id : -1;
                    var type = request.Form["Type"] ?? "";
                    var name = request.Form["Name"] ?? "";
                    var roleListJson = request.Form["RoleList"] ?? "";

                    RoleList roleList;

                    try
                    {
                        roleList = (string.IsNullOrEmpty(roleListJson))
                            ? null
                            : JsonConvert.DeserializeObject<RoleList>(roleListJson);
                    }
                    catch (Exception jsonEx)
                    {
                        roleList = null;
                    }

                    if (type == "add" && !string.IsNullOrEmpty(name) && roleList != null)
                    {
                        using (var db = new livecloudEntities1())
                        {
                            var role = new dt_ManagerRole()
                            {
                                RoleName = name,
                                LiveCmsManage = roleList.liveCmsManage.ToBoolByOnOffString(),
                                DealerManage = roleList.dealerManage.ToBoolByOnOffString(),
                                DealerList = roleList.dealerList.ToBoolByOnOffString(),
                                DealerPost = roleList.dealerPost.ToBoolByOnOffString(),
                                DealerTime = roleList.dealerTime.ToBoolByOnOffString(),
                                LiveManage = roleList.liveManage.ToBoolByOnOffString(),
                                VideoList = roleList.videoList.ToBoolByOnOffString(),
                                BarrageManage = roleList.barrageManage.ToBoolByOnOffString(),
                                SystemBarrage = roleList.systemBarrage.ToBoolByOnOffString(),
                                SystemFace = roleList.systemFace.ToBoolByOnOffString(),
                                SafeWords = roleList.safeWords.ToBoolByOnOffString(),
                                SpecialWords = roleList.specialWords.ToBoolByOnOffString(),
                                WhiteList = roleList.whiteList.ToBoolByOnOffString(),
                                BlackList = roleList.blackList.ToBoolByOnOffString(),
                                WordsManage = roleList.wordsManage.ToBoolByOnOffString(),
                                ManualReview = roleList.manualReview.ToBoolByOnOffString(),
                                GiftManage = roleList.giftManage.ToBoolByOnOffString(),
                                GiftList = roleList.giftList.ToBoolByOnOffString(),
                                DealerTable = roleList.dealerTable.ToBoolByOnOffString(),
                                Manager = roleList.Manager.ToBoolByOnOffString(),
                                ManagerList = roleList.managerList.ToBoolByOnOffString(),
                                RoleManage = roleList.roleManage.ToBoolByOnOffString(),
                                ManageLog = roleList.manageLog.ToBoolByOnOffString(),
                                AddTime = System.DateTime.Now
                            };
                            
                            db.dt_ManagerRole.Add(role);
                            db.SaveChanges();

                            result.Code = CodeConstantsHelper.RetGetDataSuc;
                            result.StrCode = CodeConstantsHelper.RetGetDataSucStr;
                            result.IsLogin = CodeConstantsHelper.RetIsLogin;
                        }
                    }
                    else if (type == "edit" && id > 0 && !string.IsNullOrEmpty(name) && roleList != null)
                    {
                        using (var db = new livecloudEntities1())
                        {
                            var role = db.dt_ManagerRole.Where(o => o.Id == id).FirstOrDefault();

                            if (role != null)
                            {
                                role.RoleName = (!string.IsNullOrEmpty(name)) ? name : role.RoleName;
                                role.LiveCmsManage = (!string.IsNullOrEmpty(roleList.liveCmsManage)) 
                                    ? roleList.liveCmsManage.ToBoolByOnOffString() 
                                    : false;
                                role.DealerManage = (!string.IsNullOrEmpty(roleList.dealerManage))
                                    ? roleList.dealerManage.ToBoolByOnOffString()
                                    : false;
                                role.DealerList = (!string.IsNullOrEmpty(roleList.dealerList))
                                    ? roleList.dealerList.ToBoolByOnOffString()
                                    : false;
                                role.DealerPost = (!string.IsNullOrEmpty(roleList.dealerPost))
                                    ? roleList.dealerPost.ToBoolByOnOffString()
                                    : false;
                                role.DealerTime = (!string.IsNullOrEmpty(roleList.dealerTime))
                                    ? roleList.dealerTime.ToBoolByOnOffString()
                                    : false;
                                role.LiveManage = (!string.IsNullOrEmpty(roleList.liveManage))
                                    ? roleList.liveManage.ToBoolByOnOffString()
                                    : false;
                                role.VideoList = (!string.IsNullOrEmpty(roleList.videoList))
                                    ? roleList.videoList.ToBoolByOnOffString()
                                    : false;
                                role.BarrageManage = (!string.IsNullOrEmpty(roleList.barrageManage))
                                    ? roleList.barrageManage.ToBoolByOnOffString()
                                    : false;
                                role.SystemBarrage = (!string.IsNullOrEmpty(roleList.systemBarrage))
                                    ? roleList.systemBarrage.ToBoolByOnOffString()
                                    : false;
                                role.SystemFace = (!string.IsNullOrEmpty(roleList.systemFace))
                                    ? roleList.systemFace.ToBoolByOnOffString()
                                    : false;
                                role.SafeWords = (!string.IsNullOrEmpty(roleList.safeWords))
                                    ? roleList.safeWords.ToBoolByOnOffString()
                                    : false;
                                role.SpecialWords = (!string.IsNullOrEmpty(roleList.specialWords))
                                    ? roleList.specialWords.ToBoolByOnOffString()
                                    : false;
                                role.WhiteList = (!string.IsNullOrEmpty(roleList.whiteList))
                                    ? roleList.whiteList.ToBoolByOnOffString()
                                    : false;
                                role.BlackList = (!string.IsNullOrEmpty(roleList.blackList))
                                    ? roleList.blackList.ToBoolByOnOffString()
                                    : false;
                                role.WordsManage = (!string.IsNullOrEmpty(roleList.wordsManage))
                                    ? roleList.wordsManage.ToBoolByOnOffString()
                                    : false;
                                role.ManualReview = (!string.IsNullOrEmpty(roleList.manualReview))
                                    ? roleList.manualReview.ToBoolByOnOffString()
                                    : false;
                                role.GiftManage = (!string.IsNullOrEmpty(roleList.giftManage))
                                    ? roleList.giftManage.ToBoolByOnOffString()
                                    : false;
                                role.GiftList = (!string.IsNullOrEmpty(roleList.giftList))
                                    ? roleList.giftList.ToBoolByOnOffString()
                                    : false;
                                role.DealerTable = (!string.IsNullOrEmpty(roleList.dealerTable))
                                    ? roleList.dealerTable.ToBoolByOnOffString()
                                    : false;
                                role.Manager = (!string.IsNullOrEmpty(roleList.Manager))
                                    ? roleList.Manager.ToBoolByOnOffString()
                                    : false;
                                role.ManagerList = (!string.IsNullOrEmpty(roleList.managerList))
                                    ? roleList.managerList.ToBoolByOnOffString()
                                    : false;
                                role.RoleManage = (!string.IsNullOrEmpty(roleList.roleManage))
                                    ? roleList.roleManage.ToBoolByOnOffString()
                                    : false;
                                role.ManageLog = (!string.IsNullOrEmpty(roleList.manageLog))
                                    ? roleList.manageLog.ToBoolByOnOffString()
                                    : false;
                                
                                db.SaveChanges();
                                
                                result.Code = CodeConstantsHelper.RetGetDataSuc;
                                result.StrCode = CodeConstantsHelper.RetGetDataSucStr;
                                result.IsLogin = CodeConstantsHelper.RetIsLogin;
                            }
                            else
                            {
                                result.Code = CodeConstantsHelper.RetParamErr;
                                result.StrCode = "edit_找不到ID:"+id;
                                result.IsLogin = CodeConstantsHelper.RetIsLogin;
                            }
                                
                        }
                    }
                    else
                    {
                        result.Code = CodeConstantsHelper.RetParamErr;
                        result.StrCode = "參數錯誤";
                        result.IsLogin = CodeConstantsHelper.RetIsLogin;
                    }
                }
                else
                {
                    result.Code = CodeConstantsHelper.RetParamErr;
                    result.StrCode = CodeConstantsHelper.UserIsNotLoginStr;
                    result.IsLogin = CodeConstantsHelper.RetIsNoLogin;

                }
                return result;
            }
            catch (Exception e)
            {
                result.Code = CodeConstantsHelper.RetParamErr;
                result.StrCode = "執行時出錯";
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                Log.Error("setPassword", "setPassword", e.ToString());

                return result;
            }
        }
    }
}
