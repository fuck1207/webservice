﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestAPI20171114.Models;
using System.Web;

namespace TestAPI20171114.Controllers
{
    public class setLiveBroadCastController : ApiController
    {
        [HttpPost]
        public ResultInfoT<object> Post()
        {
            var session = HttpContext.Current.Session;
            var request = HttpContext.Current.Request;
            var result = new ResultInfoT<object>();
            string action = request.Form["Action"] ?? "";

            try
            {
                if (session["Login"] != null && session["Login"].ToString().TrimEnd() == "1")
                {
                    var managerId = int.Parse(session["ManagerId"].ToString());

                    int liveId = -1;
                    int.TryParse(request.Form["ID"].ToString(), out liveId);
                    string text = request.Form["Content"] ?? "";

                    // 檢查參數
                    if (liveId < 0 || managerId < 0)
                    {
                        throw new Exception("自訂檢查 參數錯誤");
                    }


                    // 如果有需要, 確認管理員的等級(是否有發送廣播的權限)



                    // 發送記錄LOG (DB & WEBAPP LOG)
                    using (var db = new livecloudEntities1())
                    {
                        var broadcastLog = new dt_AdminBroadcastLog()
                        {
                            LiveId = liveId,
                            ManagerId = managerId,
                            BroadcastText = text,
                            SendTime = DateTime.Now
                        };
                        db.dt_AdminBroadcastLog.Add(broadcastLog);

                        var manageLog = new dt_ManageLog()
                        {
                            ManagerId = managerId,
                            ManagerName = session["ManagerName"].ToString(),
                            ActionType = "setLiveBroadCast",
                            AddTime = DateTime.Now,
                            Remarks = "廣播至:" + liveId + ", 內容:" + text,
                            IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]
                        };
                        db.dt_ManageLog.Add(manageLog);

                        db.SaveChanges();
                    }

                    //如果LOG DB成功, 發送給TONY 廣播彈幕API


                    result.IsLogin = CodeConstantsHelper.RetIsLogin;
                    result.Code = CodeConstantsHelper.RetGetDataSuc;
                    result.StrCode = CodeConstantsHelper.RetGetDataSucStr;
                    Log.UseLog("setLiveBroadCast", action, "");
                }
                else
                {
                    result.Code = CodeConstantsHelper.RetParamErr;
                    result.StrCode = CodeConstantsHelper.UserIsNotLoginStr;
                    result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                    Log.UseLog("setLiveBroadCast", action, "未登入");

                }
                return result;
            }
            catch (Exception e)
            {
                result.Code = CodeConstantsHelper.RetParamErr;
                result.StrCode = "執行時出錯";
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                Log.Error("setLiveBroadCast", action, e.ToString());

                return result;
            }
        }

    }
}
