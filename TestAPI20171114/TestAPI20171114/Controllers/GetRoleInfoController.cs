﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TestAPI20171114.Common;
using TestAPI20171114.Models;
using TestAPI20171114.Models.Request;

namespace TestAPI20171114.Controllers
{
    public class GetRoleInfoController : ApiController
    {
        [HttpPost]
        public ResultInfoT<object> Post()
        {
            var session = HttpContext.Current.Session;
            var request = HttpContext.Current.Request;
            var result = new ResultInfoT<object>();

            try
            {
                if (session["Login"] != null && session["Login"].ToString().TrimEnd() == "1")
                {
                    var managerId = int.Parse(session["ManagerId"].ToString());

                    int roleId = int.TryParse(request.Form["ID"].ToString(), out roleId) ? roleId : -1;

                    // 檢查參數
                    if (roleId < 0)
                    {
                        throw new Exception("自訂檢查 參數錯誤");
                    }

                    using (var db = new livecloudEntities1())
                    {
                        var query = from o in db.dt_ManagerRole
                                    where o.Id == roleId
                                    select o;

                        dt_ManagerRole mr = query.Take(1).FirstOrDefault();

                        if (mr != null)
                        {
                            var bodyData = new GetRoleInfoBodyData
                            {
                                barrageManage = mr.BarrageManage.ToOnOff(),
                                blackList = mr.BlackList.ToOnOff(),
                                dealerList = mr.DealerList.ToOnOff(),
                                dealerManage = mr.DealerManage.ToOnOff(),
                                dealerPost = mr.DealerPost.ToOnOff(),
                                dealerTable = mr.DealerTable.ToOnOff(),
                                dealerTime = mr.DealerTime.ToOnOff(),
                                giftList = mr.GiftList.ToOnOff(),
                                giftManage = mr.GiftManage.ToOnOff(),
                                livecmsManage = mr.LiveCmsManage.ToOnOff(),
                                liveManage = mr.LiveManage.ToOnOff(),
                                manageLog = mr.ManageLog.ToOnOff(),
                                Manager = mr.Manager.ToOnOff(),
                                managerList = mr.ManagerList.ToOnOff(),
                                manualReview = mr.ManualReview.ToOnOff(),
                                roleManage = mr.RoleManage.ToOnOff(),
                                safeWords = mr.SafeWords.ToOnOff(),
                                specialWords = mr.SpecialWords.ToOnOff(),
                                systemBarrage = mr.SystemBarrage.ToOnOff(),
                                systemFace = mr.SystemFace.ToOnOff(),
                                videoList = mr.VideoList.ToOnOff(),
                                whiteList = mr.WhiteList.ToOnOff(),
                                wordsManage = mr.WordsManage.ToOnOff()
                            };
                            
                            var getRoleInfo = new GetRoleInfo()
                            {
                                Id = roleId,
                                RoleName = mr.RoleName,
                                BodyData = bodyData
                            };

                            result.BackData = getRoleInfo;
                            result.IsLogin = CodeConstantsHelper.RetIsLogin;
                            result.Code = CodeConstantsHelper.RetGetDataSuc;
                            result.StrCode = CodeConstantsHelper.RetGetDataSucStr;
                        }
                        else
                        {
                            result.IsLogin = CodeConstantsHelper.RetIsLogin;
                            result.Code = CodeConstantsHelper.RetParamErr;
                            result.StrCode = "沒有找到該筆數據";
                        }
                    }
                }
                else
                {
                    result.Code = CodeConstantsHelper.RetParamErr;
                    result.StrCode = CodeConstantsHelper.UserIsNotLoginStr;
                    result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                }
                return result;
            }
            catch (Exception e)
            {
                result.Code = CodeConstantsHelper.RetParamErr;
                result.StrCode = "執行時出錯";
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                Log.Error("GetRoleInfo", "", e.ToString());

                return result;
            }
        }

    }
}
