﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TestAPI20171114.Models;


namespace TestAPI20171114.Controllers
{
    public class dt_managerController : ApiController
    {
        Models.livecloudEntities1 db = new Models.livecloudEntities1();
        //// GET: api/JJ
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET: api/JJ/5
        //public string Get(int id)
        //{

        //    return id + "value66";
        //}

        [HttpPost]
        public ResultInfoT<List<dt_dealer>> Post()
        {

            //ResultInfoT<int> result = new ResultInfoT<int>();
            ResultInfoT<List<dt_dealer>> result = new ResultInfoT<List<dt_dealer>>();
            result.Code = -1;
            result.StrCode = "登录失敗";
            result.IsLogin = false;
            ModelHelper mh = new ModelHelper();
            HttpRequest staticContext = HttpContext.Current.Request;
            string Password = "";
            string Username = "";
            string Action = "";
            Dictionary<string, string> dicFrom = new Dictionary<string, string>();
            form_dealer form_dealer = new form_dealer();
            dt_dealer dt_dealer = new dt_dealer();
            List<form_dealer> listform_dealer = new List<form_dealer>();
            listform_dealer.Add(form_dealer);
            try
            {
                if (!string.IsNullOrEmpty(staticContext.Form["Password"]))
                    Password = staticContext.Form["Password"].ToString().TrimEnd();
                if (!string.IsNullOrEmpty(staticContext.Form["Username"]))
                    Username = staticContext.Form["Username"].ToString().TrimEnd();
                if (!string.IsNullOrEmpty(staticContext.Form["Action"]))
                    Action = staticContext.Form["Action"].ToString().TrimEnd();

                if (Action == "Login")
                {
                    using (db)
                    {
                        var dt_manager = (from s in db.dt_Manager where s.user_name == Username & s.password == Password select s).FirstOrDefault();
                        if (dt_manager != null)
                        {
                            result.Code = 1;
                            result.StrCode = "登录成功";
                            result.IsLogin = true;
                            HttpContext.Current.Session["Login"] = "1";
                            return result;
                        }
                    }
                    Log.UseLog("dt_manager", Action, "Password" + Password + ";" + "Username:" + Username);
                }
                else if (Action == "LogOut")
                {
                    result.Code = 1;
                    result.StrCode = "退出成功";
                    HttpContext.Current.Session["Login"] = "0";
                    result.IsLogin = false;
                    Log.UseLog("dt_manager", Action, "");
                    return result;
                }

                //是否登入
                if (HttpContext.Current.Session["Login"] != null && HttpContext.Current.Session["Login"].ToString().TrimEnd() == "1")
                {
                    if (Action == "setDealer")
                    {
                        string Type = staticContext.Form["Type"].ToString().TrimEnd();
                        //Form轉Dic
                        dicFrom = mh.ConvertFormToDic(staticContext);
                        //Dic轉Model
                        mh.ConvertDicToModel(dicFrom, form_dealer);
                        //繫節EF
                        mh.MappingEF<form_dealer>(listform_dealer, dt_dealer);

                        dt_dealer.update_time = System.DateTime.Now;
                        if (Type == "add")
                        {
                            dt_dealer.add_time = System.DateTime.Now;
                            db.dt_dealer.Add(dt_dealer);
                            if (!ModelState.IsValid)
                            {
                                BadRequest(ModelState);
                                return result;
                            }

                            db.SaveChanges();
                            result.Code = 1;
                            result.StrCode = "成功信息";
                            Log.UseLog("dt_dealer", Action, "add:"+JsonConvert.SerializeObject(dt_dealer));
                            //result.BackData = dt_dealer;
                        }
                        else if (Type == "edit")
                        {
                            db.Configuration.ValidateOnSaveEnabled = false;
                            int ID = Convert.ToInt32(staticContext.Form["ID"].ToString());
                            using (db)
                            {
                                var dt_dealer2 = (from s in db.dt_dealer where s.id == ID select s).FirstOrDefault();
                                if (dt_dealer2 != null)
                                {
                                    dt_dealer.add_time = dt_dealer2.add_time;
                                    db.Entry(dt_dealer2).CurrentValues.SetValues(dt_dealer);
                                    db.SaveChanges();
                                    result.Code = 1;
                                    result.StrCode = "成功信息";
                                    // result.BackData = dt_dealer;
                                    Log.UseLog("dt_dealer", Action, "edit:" + JsonConvert.SerializeObject(dt_dealer));
                                }
                            }

                        }
                        else
                        {
                            return result;
                        }
                    }
                    else if (Action == "getDealer")
                    {
                        DateTime time_star = staticContext.Form["StartTime"] != null ? Convert.ToDateTime(staticContext.Form["StartTime"]) : System.DateTime.Now;
                        DateTime time_end = staticContext.Form["EndTime"] != null ? Convert.ToDateTime(staticContext.Form["EndTime"]).AddDays(1) : System.DateTime.Now;
                        string dealer_id = staticContext.Form["Code"] != null ? staticContext.Form["Code"].TrimEnd() : "";
                        string dealer_name = staticContext.Form["Name"] != null ? staticContext.Form["Name"].TrimEnd() : "";

                        List<dt_dealer> dealer_view = (from s in db.dt_dealer where s.add_time >= time_star && s.add_time <= time_end select s).ToList();

                        if (dealer_id != "")
                            dealer_view = dealer_view.Where(o => o.dealerId == dealer_id).ToList();
                        if (dealer_name != "")
                            dealer_view = dealer_view.Where(o => o.dealerName.TrimEnd() == dealer_name).ToList();

                        List<Dictionary<string, object>> listdic = new List<Dictionary<string, object>>();
                        //Model轉dic
                        mh.ConvertModelToDic<dt_dealer>(dealer_view, listdic);
                        view_dealer Model = new view_dealer();
                        mh.ConvertDicToModel<view_dealer>(listdic, Model);

                        result.BackData = dealer_view;
                        result.Code = 1;
                        result.StrCode = "成功信息";
                        result.DataCount = dealer_view.Count();
                        Log.UseLog("dt_dealer", Action, "select:" + JsonConvert.SerializeObject(dealer_view));
                        return result;
                    }
                    return result;
                }
                else
                {
                    result.StrCode = "無此session错误信息";
                    return result;
                }

                
               
            }
            catch (Exception e)
            {
                Log.Error("dt_manager", Action, "Username:" + Username + "\t" + e.ToString());
                return result;
            }

        }



    }
}
