﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TestAPI20171114.Models;
using System.Security.Cryptography;
using System.Text;

namespace TestAPI20171114.Controllers
{
    public class setDealerController : ApiController
    {
        Models.livecloudEntities1 db = new livecloudEntities1();

        [HttpPost]
        public ResultInfoT<object> Post()
        {
            var session = HttpContext.Current.Session;
            var request = HttpContext.Current.Request;
            ResultInfoT<object> result = new ResultInfoT<object>();
            
            string Action = request.Form["Action"].ToString().TrimEnd();

            try
            {
                if ((session["Login"] != null && session["Login"].ToString().TrimEnd() == "1"))
                {
                    int id = int.TryParse(request.Form["ID"] ?? "-1", out id) ? id : -1;
                    string dealerId = request.Form["Code"] ?? "";
                    string dealerName = request.Form["Name"] ?? "";
                    byte sex = Convert.ToByte(request.Form["Sex"]??"");
                    byte age = Convert.ToByte(request.Form["Age"]??"");
                    string city = request.Form["City"] ?? "";
                    decimal height = Convert.ToDecimal(request.Form["Height"] ?? "");
                    string bwh = request.Form["BWH"] ?? "";
                    decimal weight = Convert.ToDecimal(request.Form["weight"] ?? "");
                    string image = HttpContext.Current.Request.Form["Image"] ?? CodeImageHelper.DefultImg;
                    string photo = HttpContext.Current.Request.Form["Photo"] ?? CodeImageHelper.DefultImg;
                    string type = request.Form["Type"] ?? "";
                    UploadImgResultInfo uploadImg = new UploadImgResultInfo();
                    UploadImgResultInfo uploadPhoto = new UploadImgResultInfo();
                    uploadImg.State = 0;
                    uploadPhoto.State = 0;
                    if (image.Split(',').Length > 1)
                    {
                        uploadImg.FileName = image.Split(',')[0].Split('/')[1].Split(';')[0];
                        image = HttpUtility.UrlDecode(image.Split(',')[1].Trim()).Replace(" ", "+");
                        uploadImg.State = 1;
                    }

                    if (photo.Split(',').Length > 1)
                    {
                        uploadPhoto.FileName = photo.Split(',')[0].Split('/')[1].Split(';')[0];
                        photo = HttpUtility.UrlDecode(photo.Split(',')[1].Trim()).Replace(" ", "+");
                        uploadPhoto.State = 1;
                    }
                    uploadImg.FilePath = "Dealer/Image/";
                    uploadImg.Base64 = image;
                    uploadPhoto.FilePath = "Dealer/Photo/";
                    uploadPhoto.Base64 = photo;
                    string imgstr = "";
                    string photostr = "";
                    using (MD5 md5Hash = MD5.Create())
                    {
                        imgstr = GetMd5Hash(md5Hash, image.ToString());
                        photostr = GetMd5Hash(md5Hash, photo.ToString());
                    }

                    if (type == "add")
                    {
                        int lastid = 0;
                        var lastDealer = (from s in db.dt_dealer orderby s.id descending select s).Take(1).FirstOrDefault();
                        if (lastDealer != null)
                            lastid = lastDealer.id + 1;
                        int lastDealerId = lastid;
                        dt_dealer newDealer = new dt_dealer()
                        {
                            dealerId = lastDealerId.ToString().PadLeft(8, '0'),
                            dealerName = dealerName,
                            age =age,
                            sex=sex,
                            height=height,
                            bwh =bwh,                            
                            add_time =DateTime.Now,
                            update_time = DateTime.Now
                        };
                       
                        uploadImg.UploadFileName = imgstr;
                        uploadPhoto.UploadFileName = photostr;
                        string img = CodeImageHelper.updateimg(uploadImg);
                        string img2 = CodeImageHelper.updateimg(uploadPhoto);

                        if (uploadImg.State == 1 && img != "error")
                        {
                            newDealer.img = img;
                            newDealer.imgStr = imgstr;
                        }
                        
                        if (uploadPhoto.State == 1 && img2 != "error")
                        {
                            newDealer.img2 = img2;
                            newDealer.img2Str = photostr;
                        }
                            

                        db.dt_dealer.Add(newDealer);

                        var manageLog = new dt_ManageLog()
                        {
                            ManagerId = Convert.ToInt32(session["ManagerId"]),
                            ManagerName = session["ManagerName"].ToString(),
                            ActionType = "setDealer",
                            AddTime = DateTime.Now,
                            Remarks = "新增荷官:" + dealerName + "為新荷官",
                            IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]
                        };
                        db.dt_ManageLog.Add(manageLog);
                        db.SaveChanges();

                        result.IsLogin = CodeConstantsHelper.RetIsLogin;
                        result.Code = CodeConstantsHelper.RetGetDataSuc;
                        result.StrCode = CodeConstantsHelper.RetGetDataSucStr;

                    }
                    else if(type == "edit"&& id>0 )
                    {
                        var editDealer = (from s in db.dt_dealer where s.id == id select s).FirstOrDefault();
                        dt_dealer setDealer = new dt_dealer()
                        {
                            id=id,
                            dealerId = editDealer.dealerId,
                            dealerName = dealerName,
                            age = age,
                            sex = sex,
                            height = height,
                            bwh = bwh,
                            update_time = DateTime.Now,
                            img = editDealer.img,
                            img2 =editDealer.img2
                        };

                        uploadImg.UploadFileName = imgstr;
                        uploadPhoto.UploadFileName = photostr;
                        string img = CodeImageHelper.updateimg(uploadImg);
                        string img2 = CodeImageHelper.updateimg(uploadPhoto);

                        if (uploadImg.State == 1 && img != "error")
                        {
                            setDealer.img = img;
                            setDealer.imgStr = imgstr;
                        }

                        if (uploadPhoto.State == 1 && img2 != "error")
                        {
                            setDealer.img2 = img2;
                            setDealer.img2Str = photostr;
                        }

                        setDealer.add_time = editDealer.add_time;
                        db.Entry(editDealer).CurrentValues.SetValues(setDealer);

                        var manageLog = new dt_ManageLog()
                        {
                            ManagerId = Convert.ToInt32(session["ManagerId"]),
                            ManagerName = session["ManagerName"].ToString(),
                            ActionType = "setDealer",
                            AddTime = DateTime.Now,
                            Remarks = "修改荷官:" + dealerName + "資訊",
                            IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]
                        };
                        db.dt_ManageLog.Add(manageLog);

                        db.SaveChanges();




                        result.IsLogin = CodeConstantsHelper.RetIsLogin;
                        result.Code = CodeConstantsHelper.RetGetDataSuc;
                        result.StrCode = CodeConstantsHelper.RetGetDataSucStr;
                    }
                    else
                    {
                        result.IsLogin = CodeConstantsHelper.RetIsLogin;
                        result.Code = CodeConstantsHelper.RetParamErr;
                        result.StrCode = "參數錯誤";
                    }
                    return result;
                }
                else
                {
                    result.Code = CodeConstantsHelper.RetParamErr;
                    result.StrCode = CodeConstantsHelper.UserIsNotLoginStr;
                    result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                    Log.UseLog("setDealer", Action, "未登入");
                    return result;
                }
            }
            catch (Exception e)
            {
                result.Code = CodeConstantsHelper.RetParamErr;
                result.StrCode = CodeConstantsHelper.RetParamErrStr;
                result.IsLogin = CodeConstantsHelper.RetIsNoLogin;
                Log.Error("dt_dealer", Action, "");
                return result;
            }
        }
        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
    }
}
