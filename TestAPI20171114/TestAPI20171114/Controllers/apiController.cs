﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TestAPI20171114.Models;

namespace TestAPI20171114.Controllers
{
    public class apiController : ApiController
    {
        [HttpPost]
        public ResultInfoT<object> Post()
        {
            ResultInfoT<object> result = new ResultInfoT<object>();
            HttpRequest staticContext = HttpContext.Current.Request;
            string Action = "";
            if (!string.IsNullOrEmpty(staticContext.Form["Action"]))
                Action = staticContext.Form["Action"].ToString().TrimEnd();
            result.StrCode = "無此接口";
            switch (Action)
            {
                case "Login":
                    LoginController loginController = new LoginController();
                    result = loginController.Post();
                    break;
                case "LogOut":
                    LogOutController logOutController = new LogOutController();
                    result = logOutController.Post();
                    break;
                case "setDealer":
                    setDealerController setDealerController = new setDealerController();
                    result = setDealerController.Post();
                    break;
                case "getDealer":
                    getDealerController getDealerController = new getDealerController();
                    result = getDealerController.Post();
                    break;
                case "delDealer":
                    delDealerController delDealerController = new delDealerController();
                    result = delDealerController.Post();
                    break;
                case "getDealerID":
                    getDealerIDController getDealerIDController = new getDealerIDController();
                    result = getDealerIDController.Post();
                    break;
                case "getLiveList":
                    getLiveListController getLiveListController = new getLiveListController();
                    result = getLiveListController.Post();
                    break;
                case "setLiveSwitch":
                    setLiveSwitchController setLiveSwitchController = new setLiveSwitchController();
                    result = setLiveSwitchController.Post();
                    break;
                case "setLiveBroadCast":
                    setLiveBroadCastController setLiveBroadCastController = new setLiveBroadCastController();
                    result = setLiveBroadCastController.Post();
                    break;
                case "getLotteryRecord":
                    getLotteryRecordController getLotteryRecordController = new getLotteryRecordController();
                    result = getLotteryRecordController.Post();
                    break;
                case "ManagerEdit":
                    ManagerEditController ManagerEditController = new ManagerEditController();
                    result = ManagerEditController.Post();
                    break;
                case "ManageLog":
                    ManageLogController manageLogController = new ManageLogController();
                    result = manageLogController.Post();
                    break;
                case "GetRoleInfo":
                    GetRoleInfoController getRoleInfoController = new GetRoleInfoController();
                    result = getRoleInfoController.Post();
                    break;
                case "setPassword":
                    setPasswordController setPasswordController = new setPasswordController();
                    result = setPasswordController.Post();
                    break;
                case "ManageList":
                    ManageListController manageListController = new ManageListController();
                    result = manageListController.Post();
                    break;
                case "GetManager":
                    GetManagerController getManagerController = new GetManagerController();
                    result = getManagerController.Post();
                    break;
                case "setRole":
                    setRoleController setRoleController = new setRoleController();
                    result = setRoleController.Post();
                    break;
                case "ManageisLock":
                    ManageisLockController manageisLockController = new ManageisLockController();
                    result = manageisLockController.Post();
                    break;
                case "delManager":
                    delManagerController delManagerController = new delManagerController();
                    result = delManagerController.Post();
                    break;
                case "delRole":
                    delRoleController delRoleController = new delRoleController();
                    result = delRoleController.Post();
                    break;
                case "RoleManage":
                    RoleManageController roleManageController = new RoleManageController();
                    result = roleManageController.Post();
                    break;
            }
            return result;


        }


    }
}
