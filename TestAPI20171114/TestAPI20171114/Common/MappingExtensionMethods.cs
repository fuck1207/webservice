﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TestAPI20171114.Common
{
    public static class ConvertExtensionMethods
    {
        public static string ToOnOff(this bool value)
        {
            if (value)
                return "on";
            else
                return "off";
        }

        public static bool ToBoolByOnOffString(this string value)
        {
            if (value != null && value.ToLower() == "on")
                return true;
            else
                return false;
        }

    }
}