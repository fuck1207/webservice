﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Threading;


namespace TestAPI20171114
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        System.Timers.Timer liveSwitchTimer = new System.Timers.Timer();
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            liveSwitchTimer.AutoReset = false;
            liveSwitchTimer.Interval = 1000;
            liveSwitchTimer.Elapsed += new System.Timers.ElapsedEventHandler(aitoLiveSwitch_Elapsed);
            liveSwitchTimer.Start();

        }

        private void aitoLiveSwitch_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                Models.livecloudEntities1 db = new Models.livecloudEntities1();
                List<Models.dt_liveList> dt_liveList = (from s in db.dt_liveList where s.stop_time != "" select s).ToList();
                foreach (var live in dt_liveList)
                {
                    DateTime livetime = Convert.ToDateTime(live.stop_time);
                    if (livetime <= System.DateTime.Now)
                    {
                        live.state = Convert.ToByte(1);
                        live.stop_time = "";
                        live.update_time = System.DateTime.Now;
                        db.SaveChanges();
                    }
                }

            }
            catch (ThreadAbortException ex) { }
            catch (Exception ex2)
            {

            }
            finally
            {
                liveSwitchTimer.Start();
            }
        }

        protected void Application_PostAuthorizeRequest()
        {
            System.Web.HttpContext.Current.SetSessionStateBehavior(System.Web.SessionState.SessionStateBehavior.Required);
        }
    }
}
